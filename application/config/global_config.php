<?php
$config['implementasi_list'] = array("online"=>"Online" , "offline"=>"Offline");
$config['status_list'] = array("1"=>"Aktif" , "0"=>"Non Aktif");
$config['negara_list'] = array("ID"=>"Indonesia" , "My"=>"Malaysia");
$config['status_buku_list'] = array(
	"2"=>"Data Buku Masuk",
	"3"=>"Siap Di Review",
	"4"=>"Sedang Di Review",
	"5"=>"Lolos Seleksi",
	"6"=>"Tidak Lolos Seleksi",
	"7"=>"Siap Di Konversi",
	"8"=>"Sedang Di Konversi",
	"9"=>"Siap Di Beli",
);

$config['jumlah_deposit_list'] = array(
                                       "100000"=>"100000",
                                       "150000"=>"150000",
                                       "200000"=>"200000",
                                       );

$config['day_month'] = array('1' => '1',
'2' => '2',
'3' => '3',
'4' => '4',
'5' => '5',
'6' => '6',
'7' => '7',
'8' => '8',
'9' => '9',
'10' => '10',
'11' => '11',
'12' => '12',
'13' => '13',
'14' => '14',
'15' => '15',
'16' => '16',
'17' => '17',
'18' => '18',
'19' => '19',
'20' => '20',
'21' => '21',
'22' => '22',
'23' => '23',
'24' => '24',
'25' => '25',
'26' => '26',
'27' => '27',
'28' => '28',
'29' => '29',
'30' => '30'
);

$config['menu_side'] = array("dashboard" =>
									array("class" => "fa fa-home",
										  "desc" => "Dashboard",
										  "main_url" => "dashboard/",
										  "sub_menu" => ""
										  ),
									"profil" =>
									array("class" => "fa fa-bank",
										  "desc" => "Profil",
										  "main_url" => "penerbit/",
										  "sub_menu" => ""
										  ),
                                    "daftar" =>
									array("class" => "fa fa-book",
										  "desc" => "Daftar Buku",
										  "main_url" => "buku/",
										  "sub_menu" => ""
										  ),
									"invoice" =>
									array("class" => "fa fa-money",
										  "desc" => "Invoice",
										  "main_url" => "invoice/",
										  "sub_menu" => ""
										  ),
                                    "admin_user" =>
									array("class" => "fa fa-users",
										  "desc" => "Admin Pengguna",
										  "main_url" => "user/",
										  "sub_menu" => ""
										  ),
									"laporan" =>
									array("class" => "fa fa-file-text",
										  "desc" => "Laporan",
										  "main_url" => "laporan/",
										  "sub_menu" => "sub_laporan"
										  ),
									
									
									);
$config['submenu_side'] = array("sub_laporan" => array(
										"rekap_status_buku" => array(
											"class" => "fa fa-bookmark",
											"url" => "rekap_status_buku/",
											"desc" => "Rekap Status Data Buku",
										),
										"sebaran_kategori" => array(
											"class" => "fa fa-bookmark",
											"url" => "sebaran_kategori_buku/",
											"desc" => "Rekap Sebaran Kategori Buku",
										 ),
										"penjualan_buku" => array(
											"class" => "fa fa-bookmark",
											"url" => "rekap_penjualan_buku/",
											"desc" => "Rekap Penjualan Buku",
										 ),
                                         "sebaran_penjualan_buku" => array(
											"class" => "fa fa-bookmark",
											"url" => "sebaran_penjualan_buku/",
											"desc" => "Sebaran Penjualan Buku",
										 ),
										 "detail_status_buku" => array(
											"class" => "fa fa-bookmark",
											"url" => "detail_status_buku/",
											"desc" => "Detail Status Buku",
										 ),/*
                                         "sebaran_buku_per_wilayah" => array(
											"class" => "fa fa-bookmark",
											"url" => "sebaran-penjualan-buku-per-wilayah",
											"desc" => "Rekap Penyebaran Buku per Wilayah",
										 ),
                                         "sebaran_buku_per_kategori" => array(
											"class" => "fa fa-bookmark",
											"url" => "sebaran-penjualan-buku-per-kategori",
											"desc" => "Rekap Penyebaran Buku per Kategori",
										 ),*/
									),
								);

/// Config Upload Directory
$config['upload_image_path'] = TEMP_UPLOAD_DIR;
$config['extension_image_path'] = 'gif|jpg|png';
$config['ftp_folder_upload'] = array("contoh" => "Parsial" , "lengkap" =>"Complete");

?>