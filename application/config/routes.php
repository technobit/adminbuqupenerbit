<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$imageBookProof = "bookproof1|bookproof2|bookproof3";
$imageBookCover = "front|back";
/// Penerbit
$route['penerbit/simpan-data'] = 'penerbit/registerPenerbit';
$route['penerbit/update-data'] = 'penerbit/updatePenerbit';
$route['penerbit'] = 'penerbit/index';

/// Buku
$route['buku/simpan-data'] = 'buku/saveBooksData';  
$route['buku/get-list-category'] = 'buku/getListCategory';
$route['buku/validasi-upload'] = 'buku/uploadFileValidation';
$route['buku/form-upload-buku/(contoh|lengkap)/(:num)/(:num)'] = 'buku/uploadBuku/$1/$2/$3';
$route['buku/upload-buku-temp'] = 'buku/uploadTempFilePDF';
$route['buku/send-to-server'] = 'buku/bookSendToServer';
$route['buku/hapus-data-buku'] = 'buku/cancelBooks';
$route['buku/pengajuan-review-konversi'] = 'buku/filingReviewConversionBooks';

// Image Processing
$route['image/get-image-bookprof/('.$imageBookProof.')/(:num)/(:num)/(:any).jpg'] = "image/getImageBookProf/$1/$2/$3/$4";
$route['image/get-image-cover/('.$imageBookCover.')/(:num)/(:num)/(:any).jpg'] = "image/getImageCover/$1/$2/$3/$4";

$route['image/download-image-bookprof/('.$imageBookProof.')/(:num)/(:num)/(:any).jpg'] = "image/downloadImageBookProof/$1/$2/$3/$4";
$route['image/download-image-cover/('.$imageBookCover.')/(:num)/(:num)/(:any).jpg'] = "image/downloadImageCover/$1/$2/$3/$4";

// Login
$route['logout'] = 'login/logout';
$route['registrasi'] = 'penerbit/registrasi';
$route['login/lupa-password'] = 'login/lupa_password';
$route['login'] = 'login/index';

$route['dashboard'] = 'dashboard/index';
$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
