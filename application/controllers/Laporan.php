<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends MY_Controller {
    
    var $meta_title = "Laporan";
    var $meta_desc = "Laporan";
	var $main_title = "Laporan";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "B01";
    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."laporan/";
        $this->load->model("report_model");
		$this->load->model("buku_model");
    } 

	public function rekap_status_buku(){
        $menu = "B02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_rekap_status_buku(),
			"custom_js" => array(
				ASSETS_JS_URL."laporan/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}

    private function _rekap_status_buku(){
		$intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
        $statusBuku = $this->report_model->getStatusBuku($intPublisherID);
		//print_r($statusBuku);
        $arrBreadcrumbs = array(
								"Laporan" => $this->base_url,
								"Rekap Status Buku" => "#",
								);
        $dt['statusBuku'] = $statusBuku;
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $ret = $this->load->view("laporan/rekap_status_buku" , $dt , true);
        return $ret;
    }

	public function sebaran_kategori_buku(){
        $menu = "B02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_sebaran_kategori_buku(),
			"custom_js" => array(
				ASSETS_JS_URL."laporan/sebaran_kategori_buku.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    private function _sebaran_kategori_buku(){
        $listCatalog = $this->report_model->getListCatalog();
		//print_r($listBookStatus);
        $arrBreadcrumbs = array(
								"Laporan" => $this->base_url,
								"Sebaran Kategori Buku" => "#",
								);
        $dt['txtDaftarKatalog'] = $listCatalog;
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $ret = $this->load->view("laporan/sebaran_kategori_buku" , $dt , true);
        return $ret;
    }

	public function rekap_penjualan_buku(){
        $menu = "B02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_rekap_penjualan_buku(),
			"custom_js" => array(
				ASSETS_JS_URL."laporan/datepicker/bootstrap-datepicker.js",
				ASSETS_JS_URL."laporan/rekap_penjualan_buku.js",
			),
            "custom_css" => array(
				ASSETS_JS_URL."laporan/datepicker/datepicker3.css",
			),
		);	
		$this->_render("default",$dt);	
	}
    private function _rekap_penjualan_buku(){
        $arrBreadcrumbs = array(
								"Laporan" => $this->base_url,
								"Rekap Penjualan Buku" => "#",
								);
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $ret = $this->load->view("laporan/rekap_penjualan_buku" , $dt , true);
        return $ret;
    }
    
	public function sebaran_penjualan_buku(){
        $menu = "B02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_sebaran_penjualan_buku(),
			"custom_js" => array(
				ASSETS_JS_URL."laporan/sebaran_penjualan_buku.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    private function _sebaran_penjualan_buku(){
		//print_r($listBookStatus);
        $arrBreadcrumbs = array(
								"Laporan" => $this->base_url,
								"Sebaran Penjualan Buku" => "#",
								);
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $ret = $this->load->view("laporan/sebaran_penjualan_buku" , $dt , true);
        return $ret;
    }

	public function detail_status_buku($idStatus = 1){
		$menu = "B02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_detail_status_buku($idStatus),
			"custom_js" => array(
				ASSETS_JS_URL."laporan/detail_status_buku.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}

	private function _build_detail_status_buku($intStatusID){

		$arrBookStatus = array();
		$listBookStatus = $this->buku_model->getListBookStatus();
		$arrBreadcrumbs = array(
								"Laporan" => $this->base_url,
								"Sebaran Penjualan Buku" => "#",
								);
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
		foreach ($listBookStatus as $rowStatus) {
			# code...
			$arrBookStatus[$rowStatus['intStatusID']] = $rowStatus['txtStatus'];
		}
		$dt['frmBookStatus'] = $this->form_builder->inputDropdown("Status Buku" , "intStatusID" , $intStatusID , $arrBookStatus);
        $ret = $this->load->view("laporan/detail_status_buku" , $dt , true);
        return $ret;		

	}
	
	public function loadBookList(){
		///Parameter To Post
		$intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
		$intCatalogID = $this->input->post("intCatalogID");
		$intCategoryID = $this->input->post("intCategoryID");
		////
		$bookList = $this->report_model->getRecapBookCategory($intPublisherID,$intCatalogID,$intCategoryID);
		$retVal['data'] = array();
		if(!empty($bookList)){
			foreach ($bookList as $rowBuku) {
				# code...
				$arrData = array(
					$rowBuku['txtCategory'],
					$rowBuku['txtStatus'],
					$rowBuku['intBookCount'],
				);
				$retVal['data'][] = $arrData;
			}
		}
		echo json_encode($retVal);
	}
	
	public function loadBookSeller(){
		///Parameter To Post
		$intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
		$date1 = $this->input->post("date1");
		$dtStart = date("Y-m-d", strtotime($date1));
		$date2 = $this->input->post("date2");
		$dtEnd = date("Y-m-d", strtotime($date2));
		////
		$retVal['data'] = array();
		$bookSeller = $this->report_model->getRecapBookSeller($intPublisherID,$dtStart,$dtEnd);
		$totalCopyLicense = 0;
		$totalPurchase = 0;
		if(!empty($bookSeller)){
			foreach ($bookSeller as $key){
				$stringRp = 'Rp. '.number_format($key['curTotalPurchase'],0,".",".").'';
				$arrData = array(
					$key['intYear'],
					$key['intMonth'],
					$key['intCopyLicense'],
					$stringRp	
				);
				$retVal['data'][] = $arrData;
				$totalCopyLicense += $key['intCopyLicense'];
				$totalPurchase += $key['curTotalPurchase'];
				
				
			}
			$stringRp = 'Rp. '.number_format($totalPurchase , "0", "." , ".").'';
			$retVal['data'][] = array(
					"Total",
					"",
					$totalCopyLicense,
					$stringRp
				);	

		}
		echo json_encode($retVal);
	}

	public function loadSebaranBuku(){
		///Parameter To Post
		$intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
		$txtBookTitle = $this->input->post("txtBookTitle");
		////
		$bookList = $this->report_model->getSebaranBuku($intPublisherID,$txtBookTitle);
		$retVal['data'] = array();
		
		if(!empty($bookList)){
			foreach ($bookList as $key){
				$arrData = array(
								$key['txtCatalog'] , 
								$key['txtCategory'] , 
								$key['txtLibraryName'] ,
								$key['txtBookTitle'] 
								);
				$retVal['data'][] = $arrData;
			}
		}
		
		echo json_encode($retVal);
	}

	public function getDetailStatusBuku(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegal!";die;
		}

		$intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
		$intStatusID = $this->input->post("intStatusID");
		$retVal['data'] = array();
		$listBuku = $this->report_model->getDetailStatusBuku($intPublisherID , $intStatusID);
		foreach ($listBuku as $rowBuku) {
			# code...
			$arrData = array(
				$rowBuku['txtCatalog'],
				$rowBuku['txCategory'],
				$rowBuku['txtISBN'],
				$rowBuku['txtBookTitle'],
				$rowBuku['txtStatus'],
			);
		$retVal['data'][] = $arrData;
		}
		echo json_encode($retVal);
	}
}