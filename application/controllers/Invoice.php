<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MY_Controller {
    var $meta_title = "Invoice";
    var $meta_desc = "Invoice Perpustakaan";
	var $main_title = "Login";
    var $base_url = "";
	var $upload_dir = TEMP_UPLOAD_DIR;
	var $upload_url = URL_UPLOAD_DIR;
	var $limit = "10";
    
    public function __construct(){
        parent::__construct();
        $this->base_url = base_url()."invoice/";
		$this->load->model("user_model");
        $this->load->model("invoice_model");
		$this->lang->load('id_site', 'id');
		$this->load->library('upload'); 
    }
    
	public function index()
	{
        /// invoice
        $menu = "B01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_invoice(),
			"custom_js" => array(
                ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."invoice/form.js",
                
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    private function _build_invoice(){
        
        $idPenerbit = $this->session->userdata("pcw_penerbit_publisher_id");        
        $listInvoice = $this->invoice_model->getListInvoice($idPenerbit);

        $arrBreadcrumbs = array(
								"Invoice" => $this->base_url,
								"List Invoice" => "#",
								);
		$dt['url'] = $this->base_url;
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $dt['listInvoice'] = $listInvoice;
		//print_r($listInvoice);
        $ret = $this->load->view("invoice/list" , $dt , true);
        return $ret;
    }
	
	public function detail($intYear, $intMonth){
        /// invoice
        $menu = "B01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_detail_invoice($intYear, $intMonth),
			"custom_js" => array(
                //ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."invoice/content.js",
                
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    private function _build_detail_invoice($intYear, $intMonth){
        
        $idPenerbit = $this->session->userdata("pcw_penerbit_publisher_id");        
        $detailInvoice = $this->invoice_model->getDetailInvoice($idPenerbit, $intYear, $intMonth);
 		$detailInvoicePurchase = $this->invoice_model->getDetailInvoicePurchase($idPenerbit, $intYear, $intMonth);
 		
        $arrBreadcrumbs = array(
								"Invoice" => $this->base_url,
								"Detail Invoice" => "#",
								);
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $dt['detailInvoice'] = $detailInvoice;
        $dt['detailInvoicePurchase'] = $detailInvoicePurchase;
		//print_r($detailInvoice);
        $ret = $this->load->view("invoice/detail" , $dt , true);
        return $ret;
    }
	
	public function request($intYear, $intMonth, $intCopyLicense, $curTotalPurchase){
        /// invoice
        $menu = "B01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_request_invoice($intYear, $intMonth, $intCopyLicense, $curTotalPurchase),
			"custom_js" => array(
                //ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."invoice/form.js",
                
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    private function _build_request_invoice($intYear, $intMonth, $intCopyLicense, $curTotalPurchase){
        
        $idPenerbit = $this->session->userdata("pcw_penerbit_publisher_id");        
        //$detailInvoice = $this->invoice_model->getDetailInvoice($idPenerbit, $intYear, $intMonth);
 		$detailInvoicePurchase = $this->invoice_model->getDetailInvoicePurchase($idPenerbit, $intYear, $intMonth);
 		
        $arrBreadcrumbs = array(
								"Invoice" => $this->base_url,
								"Ajukan Invoice" => "#",
								);
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $dt['intYear'] = $intYear;
        $dt['intMonth'] = $intMonth;
        $dt['intCopyLicense'] = $intCopyLicense;
        $dt['curTotalPurchase'] = $curTotalPurchase;
        $dt['detailInvoicePurchase'] = $detailInvoicePurchase;
		//print_r($detailInvoice);
        $ret = $this->load->view("invoice/request" , $dt , true);
        return $ret;
    }

        public function requestInvoice(){
            $ntxtInvoiceImgDir = $this->input->post("ntxtInvoiceImgTxtDir");

            $ntxtImage = "";
            if(!empty($ntxtInvoiceImgDir)){
                $imgInvoiceImg = $this->input->post("imgInvoiceImg");
                $extensionFile = getExtensionFile($imgInvoiceImg);
                $ntxtImage = file_get_contents($ntxtInvoiceImgDir);
                $ntxtImage = "image/".$extensionFile.";".base64_encode($ntxtImage);
            }

            $intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
            $txtInvoiceNumber = $this->input->post("txtInvoiceNumber");
            $intCopyLicense = $this->input->post("intCopyLicense");
            $curTotalPurchase = $this->input->post("curTotalPurchase");
            $intYear = $this->input->post("intYear");
            $intMonth = $this->input->post("intMonth");
            $ntxtInvoiceProof = $ntxtImage;



            $requestInvoice = $this->invoice_model->requestInvoice($intPublisherID,$txtInvoiceNumber,$intCopyLicense,$curTotalPurchase,$ntxtInvoiceProof,$intYear,$intMonth);

            $res = $requestInvoice;
		    echo json_encode($res);
        }

	public function uploadToConvert(){

		if(!$this->input->is_ajax_request()){
			echo "Ilegal Upload";die;
		}

		$retVal = array();
		///$this->load->library("upload");
		$fileImage = $_FILES['fileImage'];
		$nameFile = $fileImage["name"];
		$imagefile = explode("." , $nameFile);
        $extImage = "." .end($imagefile);
        $postName = str_replace($extImage ,"",$nameFile);
		$postName = $postName.date("YmdHis");

		$config['upload_path'] = ASSETS_UPLOAD_DIR;
		$config['allowed_types'] = 'gif|jpg';
		$config['file_name'] = $postName;
		$this->upload->initialize($config);
		$resUpload = $this->upload->do_upload("fileImage");
		if(!$resUpload){
			$retVal['status'] = $resUpload;
			$retVal['message'] = $this->upload->display_errors();
		}else{
			$data_upload = $this->upload->data();
			$file_dir = $data_upload['full_path'];
			$image_temp_url = str_replace(TEMP_UPLOAD_DIR , URL_UPLOAD_DIR , $file_dir);
			$retVal['status'] = true;
			$retVal['message'] = "File Berhasil Di Upload";
			$retVal['temp_image_url'] = $image_temp_url;
			$retVal['temp_image_dir'] = $file_dir;
		}
		echo json_encode($retVal);
	}
    
}
