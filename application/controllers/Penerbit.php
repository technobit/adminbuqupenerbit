<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerbit extends MY_Controller {
    var $meta_title = "Penerbit";
    var $meta_desc = "Penerbit Perpustakaan";
	var $main_title = "Login";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
    
    public function __construct(){
        parent::__construct();
        $this->base_url = base_url()."login/";
		$this->load->model("user_model");
        $this->load->model("penerbit_model");
		$this->lang->load('id_site', 'id');
    }
    
	public function index()
	{
        /// Profil Penerbit
        $menu = "B01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_penerbit(),
			"custom_js" => array(
                ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."penerbit/form.js",
                
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    private function _build_penerbit(){
        
        $idPenerbit = $this->session->userdata("pcw_penerbit_publisher_id");        
        $detailPenerbit = $this->penerbit_model->detailPenerbit($idPenerbit);
        
        $detailPenerbit = $detailPenerbit[0];
        ///echopre($detailPenerbit);die;
        
        $listBankAccount = $this->penerbit_model->getListBank();
        ///echopre($listBankAccount);die;
        $arrBank = array();
        foreach ($listBankAccount as $key => $value) {
            # code...
            $arrBank[$value['txtBankName']] = $value['txtBankName'];
        }
        $dt = array();
        
        $dt['intPublisherID'] = $this->form_builder->inputHidden('intPublisherID' , $detailPenerbit['intPublisherID']);
        $dt['txtPublisherGroup'] = $this->form_builder->inputText('Nama Perusahaan / Badan','txtPublisherGroup' , $detailPenerbit['txtPublisherGroup']);
        $dt['txtPublisherName'] = $this->form_builder->inputText('Nama Penerbit','txtPublisherName' , $detailPenerbit['txtPublisherName']);
        $dt['txtCountry'] = $this->form_builder->inputText('Negara' ,'txtCountry' ,  "Indonesia" , "col-sm-3" , array("readonly"=>"readonly"));
        $dt['txtProvince'] = $this->form_builder->inputText('Provinsi' ,'txtProvince' , $detailPenerbit['txtProvince']);
        $dt['txtCity'] = $this->form_builder->inputText('Kota' ,'txtCity' ,  $detailPenerbit['txtCity']);
        $dt['txtAddress'] = $this->form_builder->inputTextArea('Alamat' ,'txtAddress' ,  $detailPenerbit['txtAddress']);
        $dt['txtPostalCode'] = $this->form_builder->inputText('Kode Pos' ,'txtPostalCode' ,  $detailPenerbit['txtPostalCode']);
        $dt['txtEmail'] = $this->form_builder->inputText( 'Email' ,'txtEmail' , $detailPenerbit['txtEmail']);
        $dt['txtPhoneNumber'] = $this->form_builder->inputText('Nomor Telpon' ,'txtPhoneNumber' ,  $detailPenerbit['txtPhoneNumber']);
        $dt['txtFax'] = $this->form_builder->inputText('No Fax' ,'txtFax' , $detailPenerbit['txtFax']);
        $dt['txtURLWebsite'] = $this->form_builder->inputText('Website' ,'txtURLWebsite' ,  $detailPenerbit['txtURLWebsite']);

        $dt['txtNPWP'] = $this->form_builder->inputText( 'NPWP' ,'txtNPWP' , $detailPenerbit['txtNPWP']);
        $dt['txtContactOwner'] = $this->form_builder->inputText( 'Kontak' ,'txtContactOwner' , $detailPenerbit['txtContactOwner']);
        $dt['txtPhoneNumberOwner'] = $this->form_builder->inputText( 'Nomor Telpon' ,'txtPhoneNumberOwner' , $detailPenerbit['txtPhoneNumberOwner']);
        $dt['txtEmailOwner'] = $this->form_builder->inputText( 'Email' ,'txtEmailOwner' , $detailPenerbit['txtEmailOwner']);
         
        $dt['txtContact1'] = $this->form_builder->inputText( 'Kontak 1' ,'txtContact1' , $detailPenerbit['txtContact1']);
        $dt['txtPhoneNumber1'] = $this->form_builder->inputText( 'Nomor Telpon' ,'txtPhoneNumber1' , $detailPenerbit['txtPhoneNumber1']);
        $dt['txtEmail1'] = $this->form_builder->inputText( 'Email' ,'txtEmail1' , $detailPenerbit['txtEmail1']);
        
        $dt['txtContact1'] = $this->form_builder->inputText( 'Kontak 1' ,'txtContact1' , $detailPenerbit['txtContact1']);
        $dt['txtPhoneNumber1'] = $this->form_builder->inputText( 'Nomor Telpon' ,'txtPhoneNumber1' , $detailPenerbit['txtPhoneNumber1']);
        $dt['txtEmail1'] = $this->form_builder->inputText( 'Email' ,'txtEmail1' , $detailPenerbit['txtEmail1']);
        
        $dt['txtContact2'] = $this->form_builder->inputText( 'Kontak 2' ,'txtContact2' , $detailPenerbit['txtContact2']);
        $dt['txtPhoneNumber2'] = $this->form_builder->inputText( 'Nomor Telpon' ,'txtPhoneNumber2' , $detailPenerbit['txtPhoneNumber2']);
        $dt['txtEmail2'] = $this->form_builder->inputText( 'Email' ,'txtEmail2' , $detailPenerbit['txtEmail2']);
        
        $arrChecked = array();
        
        if(!empty($detailPenerbit['bitIKAPI'])){
            $arrChecked[] = "bitIKAPI"; 
        }
        
        if(!empty($detailPenerbit['bitAPPTI'])){
            $arrChecked[] = "bitAPPTI"; 
        }
        
        if(!empty($detailPenerbit['bitOther'])){
            $arrChecked[] = "bitOther"; 
        }
        
        $arrForm = array("bitAPPTI" => "APPTI" , "bitIKAPI" => "IKAPI" , "bitOther"=>"Other");
        $dt['txtAnggota'] = $this->form_builder->inputCheckboxGroup('Keanggotaan Asosiasi' ,'txtAnggota' , $arrChecked  , $arrForm);
        
        $dt['txtBankName'] = $this->form_builder->inputText('Nama Bank' ,'txtBankName' ,  "Mandiri" , "col-sm-3" , array("readonly"=>"readonly"));
        $dt['txtBankAccount'] = $this->form_builder->inputText( 'No Rekening' ,'txtBankAccount' , $detailPenerbit['txtBankAccount']);
        $dt['txtBankUserName'] = $this->form_builder->inputText( 'Atas Nama' ,'txtBankUserName' , $detailPenerbit['txtBankUserName']);
        
        $dt['txtBankNameOps'] = $this->form_builder->inputDropdown('Nama Bank' ,'txtBankNameOps' ,  $detailPenerbit['txtBankNameOps'],$arrBank );
        $dt['txtNoBankAccountOps'] = $this->form_builder->inputText( 'No Rekening' ,'txtNoBankAccountOps' , $detailPenerbit['txtNoBankAccountOps']);
        $dt['txtBankUserNameOps'] = $this->form_builder->inputText( 'Atas Nama' ,'txtBankUserNameOps' , $detailPenerbit['txtBankUserNameOps']);
        
        $arrBreadcrumbs = array(
								"Penerbit" => $this->base_url,
								"Profil Penerbit" => "#",
								);
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        
        $ret = $this->load->view("penerbit/form" , $dt , true);
        return $ret;
    }
	
    public function registrasi(){
        $dt = array();
        $this->form_builder->form_type = "form-inline";
        $dt['txtPublisherGroup'] = $this->form_builder->inputText('Nama Perusahaan / Badan','txtPublisherGroup' , "");
        $dt['txtPublisherName'] = $this->form_builder->inputText('Nama Penerbit','txtPublisherName' , "");
        $dt['txtCountry'] = $this->form_builder->inputText('Negara' ,'txtCountry' ,  "Indonesia" , "col-sm-3" , array("readonly"=>"readonly"));
        $dt['txtProvince'] = $this->form_builder->inputText('Provinsi' ,'txtProvince' , "");
        $dt['txtCity'] = $this->form_builder->inputText('Kota' ,'txtCity' ,  "");
        $dt['txtAddress'] = $this->form_builder->inputTextArea('Alamat' ,'txtAddress' ,  "");
        $dt['txtPostalCode'] = $this->form_builder->inputText('Kode Pos' ,'txtPostalCode' ,  "");
        $dt['txtEmail'] = $this->form_builder->inputText( 'Email' ,'txtEmail' , "");
        $dt['txtPhoneNumber'] = $this->form_builder->inputText('Nomor Telpon' ,'txtPhoneNumber' ,  "");
        $dt['txtFax'] = $this->form_builder->inputText('No Fax' ,'txtFax' , "");
        $dt['txtURLWebsite'] = $this->form_builder->inputText('Website' ,'txtURLWebsite' ,  "");
        $arrForm = array("bitAPPTI" => "APPTI" , "bitIKAPI" => "IKAPI" , "bitOther"=>"Other");
        $dt['txtAnggota'] = $this->form_builder->inputCheckboxGroup('Keanggotaan Asosiasi' ,'txtAnggota' , array()  , $arrForm);
        /// Login Admin
        $dt['txtUsername'] = $this->form_builder->inputText('Username' ,'txtUserName' ,  "");
        $dt['txtEmailAdmin'] = $this->form_builder->inputText('Email' ,'txtEmailAdmin' , "");
        $dt['txtPassword'] = $this->form_builder->inputText('Password' ,'txtPassword' , "");
        $dt['cancel_link'] = $this->base_url_site."login/";
        $this->load->view("penerbit/registrasi" , $dt);
    }
    
    public function registerPenerbit(){
        if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		$status = false;
		$message = "Registrasi Penerbit Gagal";
        $resVal = $this->penerbit_model->registrationPubliser($this->input->post());
        $statusRes = $resVal[0]['bitSuccess'];
        
        if($statusRes==1){
            $status = true;
            $message = "Registrasi Penerbit Berhasil";
        }else{
            $status = false;
            $message = "Registrasi Penerbit Gagal";
        }
        $retVal['status'] = $status;
        $retVal['messsage'] = $message;
        echo json_encode($retVal);
    }
    
    public function updatePenerbit(){
        if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		$status = false;
		$message = "Data Gagal Di Gagal";
        $resVal = $this->penerbit_model->updatePenerbit($this->input->post());
        
        $statusRes = $resVal[0]['bitSucces'];
        
        if($statusRes==1){
            $status = true;
            $message = "Data Berhasil Di Simpan";
        }else{
            $status = false;
            $message = "Data Gagal Di Gagal";
        }
        $retVal['status'] = $status;
        $retVal['message'] = $message;
        echo json_encode($retVal);
    }
}
