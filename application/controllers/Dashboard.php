<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    var $meta_title = "Dashboard";
    var $meta_desc = "Dashboard";
	var $main_title = "Dashboard";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "D01";
    public function __construct(){
        parent::__construct();
        $this->load->model("dashboard_model");
        $this->base_url = $this->base_url_site."dashboard/";
        
    } 
    
	public function index()
	{
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_home(),
			"custom_js" => array(
				
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
	
    private function _home(){
        
        $dt = array();
        $link_report = $this->base_url_site."laporan/detail_status_buku/";
        $intIdPublisher = $this->session->userdata("pcw_penerbit_publisher_id");
        $bukuDiajukan= $this->dashboard_model->getBukuDiajukan($intIdPublisher);
        $BukuSiapDiReview = $this->dashboard_model->getBukuSiapDiReview($intIdPublisher);
        $BukuDiReview = $this->dashboard_model->getBukuSedangDiReview($intIdPublisher);
        $BukuLolosSeleksi = $this->dashboard_model->getBukuLolosSeleksi($intIdPublisher);
        $BukuTakLolosSeleksi = $this->dashboard_model->getBukuTakLolosSeleksi($intIdPublisher);
        $BukuSiapKonversi = $this->dashboard_model->getBukuSiapDiKonversi($intIdPublisher);
        $BukuSedangDiKonversi = $this->dashboard_model->getBukuSedangDiKonversi($intIdPublisher);
        $BukuSiapDiBeli = $this->dashboard_model->getBukuSiapDiBeli($intIdPublisher);

        $arrDashboard = array(
            "diajukan"=> array(
                "label" => "Buku Di Ajukan",
                "icon" => "fa fa-book",
                "link" => $link_report."2",
                "data" =>  $bukuDiajukan 
            ),
            "siap_direview"=> array(
                "label" => "Buku Siap Di Review",
                "icon" => "fa fa-check-square-o",
                "link" => $link_report."3",
                "data" =>  $BukuSiapDiReview 
            ),
            "direview"=> array(
                "label" => "Buku Sedang Di Review",
                "icon" => "fa fa-pencil-square-o",
                "link" => $link_report."4",
                "data" =>  $BukuDiReview 
            ),
            "lolos_seleksi"=> array(
                "label" => "Buku Lolos Seleksi",
                "icon" => "fa fa-check-square",
                "link" => $link_report."5",
                "data" =>  $BukuLolosSeleksi 
            ),
            "tidak_lolos_seleksi"=> array(
                "label" => "Buku Tidak Lolos Seleksi",
                "icon" => "fa fa-ban",
                "link" => $link_report."6",
                "data" =>  $BukuTakLolosSeleksi 
            ),
            "siap_konversi"=> array(
                "label" => "Buku Siap Di Konversi",
                "icon" => "fa fa-sign-out",
                "link" => $link_report."7",
                "data" =>  $BukuSiapKonversi
            ),
            "sedang_konversi"=> array(
                "label" => "Buku Sedang Di Konversi",
                "icon" => "fa fa-clone",
                "link" => $link_report."8",
                "data" =>  $BukuSedangDiKonversi 
            ),
            "siap_beli"=> array(
                "label" => "Buku Siap Di Beli",
                "icon" => "fa fa-shopping-cart",
                "link" => $link_report."9",
                "data" =>  $BukuSiapDiBeli 
            ),
        );
        $dt['arrDashboard'] = $arrDashboard; 
        $ret = $this->load->view("dashboard/content" , $dt , true);
        return $ret;
    }
	
	

	
	
}
