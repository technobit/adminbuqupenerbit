<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
    var $meta_title = "Login";
    var $meta_desc = "Admin Perpustakaan";
	var $main_title = "Login";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
    
    public function __construct(){
        parent::__construct();
        $this->base_url = base_url()."login/";
		$this->load->model("user_model");
		$this->lang->load('id_site', 'id');
    }
    
	public function index()
	{
        $res = "";
        if(isset($_POST['submit'])){
            $res = $this->checkLogin();
        }

        $dt["alert"] = $res;
		$dt["forget_link"] = $this->base_url."lupa-password/";
		$dt['link_registrasi'] = $this->base_url_site."registrasi/";
		$this->load->view("login/content" , $dt);
	}
	
    public function checkLogin(){
		
        $res = array();
        $email = $this->security->xss_clean($this->input->post("email"));
        $password = $this->security->xss_clean($this->input->post("password"));
		$resVal = $this->user_model->get_user_login($email , $password);
		if(count($resVal) < 1){
			$retVal['status'] = false;
			$retVal['message'] = "Internet Gagal";
			return $retVal;
			exit();
		}
		
		$retuser = $resVal[0];
		if($retuser['bitSuccess']==0){
			$retVal['status'] = false;
			$retVal['message'] = "Login Gagal, Cek Email / Password Anda";
			return $retVal;
			exit();
		}else{			
			if($retuser['bitActive']==1){
				$arrSession = array(
								"pcw_penerbit_username" => $retuser["txtUserName"],
                                "pcw_penerbit_email" => $retuser["txtEmail"],
								"pcw_penerbit_is_super_admin" => $retuser["bitSuperAdmin"],
                                "pcw_penerbit_bit_active" => $retuser["bitActive"],
								"pcw_penerbit_publisher_id" => $retuser["intPublisherID"],
								"pcw_penerbit_publisher_userid" => $retuser["intPublisherUserID"],
								"pcw_penerbit_publisher_name" => $retuser["intPublisherName"],
								"pcw_penerbit_user_validated" => true,
								"pcw_penerbit_user_language" => "ID", /// Default To Indonesian,  Or Gabon Maybe
								"pcw_penerbit_ftp_host" => $retuser['txtFTPHost'],
								"pcw_penerbit_ftp_username" => $retuser['txtFTPUsername'],
								"pcw_penerbit_ftp_password" => $retuser['txtFTPPassword'],
								"pcw_penerbit_ftp_folder" => $retuser['txtFTPFolder'],
								);
					$redirect_url = base_url()."dashboard/";
					$this->session->set_userdata($arrSession);
                    redirect($redirect_url);
			}else{
				$retVal['status'] = false;
				$retVal['message'] = "Akun Anda Tidak Aktif";
				return $retVal;
				exit();
			}
		}
    }

	public function lupa_password(){
		$dt = array();
		$res = "";
        if(isset($_POST['submit'])){
            $res = $this->_check_forget_password();
        }
        $dt["alert"] = $res;
		$this->load->view("login/forget" , $dt);
	}

	public function _check_forget_password(){
		
		$publisher = $this->security->xss_clean($this->input->post("txtPublisherName"));
		$email = $this->security->xss_clean($this->input->post("txtEmailPublisher"));
		$resPassword = $this->user_model->get_forget_password($email , $publisher);
		$dataPassword = $resPassword[0];
		$status = $dataPassword['bitSuccess']==1 ? true : false;
		$message = "Password baru Berhasil Di Kirim";
		if($status){
			$ret = $this->sendEmail($dataPassword , $publisher);
			if($ret){
				$status = true;
				$message = "Password baru Berhasil Di Kirim Ke Email Anda";		
			}else{
				$status = false;
				$message = "Password baru Gagal Di Kirim Ke Email Anda";
			}
		}else{
		$status = false;
		$message = "Password baru Gagal Di Kirim (".$dataPassword['txtInfo'].")";
		}
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		return $retVal;
	}

	private function sendEmail($dataPassword , $publisher){
		
		$mailServer = "hasanudin@technobit.id";
		$mailSender = "noreply";
		$newPassword = $dataPassword['txtNewPassword'];
		$messageEmail = "Hai ".$publisher." \n\n\n Password baru Anda Adalah ".$newPassword." \n\n\nTerima Kasih";
		$this->load->library("email");
		$this->email->from($mailServer, $mailSender);
		$this->email->to($dataPassword['txtEmail']);
		$this->email->subject('Password Baru Buqu Admin Dispenser');
		$this->email->message($messageEmail);
		$this->email->set_newline("\r\n");
		$ret = $this->email->send();
		$string = $this->email->print_debugger();
		return $ret;
	}
	
    public function logout(){
		$this->session->sess_destroy();
		redirect("/");
	}
    	
}
