<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {
    
    var $meta_title = "User";
    var $meta_desc = "User";
	var $main_title = "User";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "B01";
    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."user/";
        $this->load->model("user_model");
    } 
    
	public function index(){
        $menu = "B01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_user_list(),
			"custom_js" => array(ASSETS_JS_URL."user/content.js"),
            "custom_css" => array(),
		);	
		$this->_render("default",$dt);	
	}
    private function _build_user_list(){
        
        $idPenerbit = $this->session->userdata("pcw_penerbit_publisher_id");        
        $listUser = $this->user_model->getAdminList($idPenerbit);

        $arrBreadcrumbs = array(
								"User" => $this->base_url,
								"List User" => "#",
								);
		$dt['url'] = $this->base_url;
		$dt['breadcrumbs'] = $this->setBreadcrumbs($arrBreadcrumbs);
        $dt['listUser'] = $listUser;
		//print_r($listUser);
        $ret = $this->load->view("user/list" , $dt , true);
        return $ret;
    }

//AJAX===============

        public function addUser(){
            $intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
            $addTxtUserName = $this->input->post("addTxtUserName");
            $addTxtEmail = $this->input->post("addTxtEmail");
            $addTxtPassword = $this->input->post("addTxtPassword");

            $addNewUser = $this->user_model->addNewUser($intPublisherID,$addTxtUserName,$addTxtEmail,$addTxtPassword);

            $res = $addNewUser;
		    echo json_encode($res);
        }

        public function removeUser(){
            $intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
            $intPublisherUserID = $this->input->post("hapusUserID");

            $removeUser = $this->user_model->removeUser($intPublisherID,$intPublisherUserID);

            $res = $removeUser;
            //$res = ''.$intPublisherID.','. $intPublisherUserID.'';
		    echo json_encode($res);
        }

        public function nonactiveUser(){
            $intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
            $intPublisherUserID = $this->input->post("nonaktifUserID");

            $nonactiveUser = $this->user_model->nonactiveUser($intPublisherID,$intPublisherUserID);

            $res = $nonactiveUser;
		    echo json_encode($res);
        }

        public function activeUser(){
            $intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
            $intPublisherUserID = $this->input->post("aktifUserID");

            $activeUser = $this->user_model->activeUser($intPublisherID,$intPublisherUserID);

            $res = $activeUser;
		    echo json_encode($res);
        }
        public function editUser(){
            $intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
            $intPublisherUserID = $this->input->post("ubahUserID");
            $txtUserName = $this->input->post("ubahTxtUserName");
            $txtEmail = $this->input->post("ubahTxtEmail");

            $editUser = $this->user_model->editUser($intPublisherID,$intPublisherUserID,$txtUserName,$txtEmail);

            $res = $editUser;
		    echo json_encode($res);
        }

        public function editPass(){
            $intPublisherID = $this->session->userdata("pcw_penerbit_publisher_id");
            $intPublisherUserID = $this->input->post("ubahPassUserID");
            $txtEmail = $this->input->post("ubahPassTxtEmail");
	        $txtOldPassword = $this->input->post("txtPasswordOld");
	        $txtNewPassword = $this->input->post("txtPasswordNew");

            $editPass = $this->user_model->editPass($intPublisherID,$intPublisherUserID,$txtEmail,$txtOldPassword,$txtNewPassword);

            $res = $editPass;
		    echo json_encode($res);
        }
}
