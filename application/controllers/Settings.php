<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {
    
    var $meta_title = "Katalog";
    var $meta_desc = "Katalog";
	var $main_title = "Katalog";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "K01";
    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."katalog/";
    } 
    
	public function index()
	{
        $menu = "K01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_data_katalog(),
			"custom_js" => array(
				
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function detail_katalog($id)
	{
        $menu = "K02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_detail_katalog(),
			"custom_js" => array(
				
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
   
    private function _data_katalog(){
        $dt = array();
        $ret = $this->load->view("katalog/content" , $dt , true);
        return $ret;
    }
    
    
    private function _detail_katalog($id){
        $dt = array();
        $ret = $this->load->view("katalog/detail" , $dt , true);
        return $ret;
    }
	
}
