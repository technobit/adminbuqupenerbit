<?php

    class Report_model extends MY_Model{
        #code
        
        function __construct(){
            
        }
        
        function getStatusBuku($intPublisherID){
            $sp_name = "PublisherDisp_RptRecapStatusDataBuku";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function getListCatalog(){
            $sp_name = "PublisherDisp_CatalogRetrieve";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function getListCategory($intCatalogID){
            $sp_name = "PublisherDisp_CategoryRetrieve";
            $arrPost = array();
			$arrPost['intCatalogID'] = $intCatalogID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function getRecapBookCategory($intPublisherID,$intCatalogID,$intCategoryID){
            $sp_name = "PublisherDisp_RptRecapStatusDataBukuPerCategory";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intCatalogID'] = $intCatalogID;
			$arrPost['intCategoryID'] = $intCategoryID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function getRecapBookSeller($intPublisherID,$dtStart,$dtEnd){
            $sp_name = "PublisherDisp_RptRecapBookSelling";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['dtStart'] = $dtStart;
			$arrPost['dtEnd'] = $dtEnd;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function getSebaranBuku($intPublisherID,$txtBookTitle){
            $sp_name = "PublisherDisp_RptSebaranBuku";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['txtBookTitle'] = $txtBookTitle;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getDetailStatusBuku($intPublisherID,$intStatusID){
            $sp_name = "PublisherDisp_RptDetailStatusDataBuku";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intStatusID'] = $intStatusID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
	}
?>
