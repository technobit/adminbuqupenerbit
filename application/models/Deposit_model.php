<?php
    class Deposit_Model extends MY_Model{
        #code
        
        function __construct(){
            
        }
        
        function getLibraryDeposit($post){
            $sp_name = "LibDisp_Internal_LibrarySearchForDeposit";
            $arrPost = array();
            $arrPost['txtDispenserType'] = !empty($post['txtDispenserType']) ? $post['txtDispenserType'] : "";
            $arrPost['intLibraryType'] = $post['intLibraryType'];
            $arrPost['txtLibraryName'] = $post['txtLibraryName'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function detailLibraryDeposit($id_library){
            $sp_name = "LibDisp_Internal_LibraryDetailGetDeposit";
            $arrPost = array();
            $arrPost['intLibraryID'] = $id_library;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function listTopUpDeposit($tipe_library){
            $sp_name = "LibDisp_Internal_GetDepositListOnLibraryType";
            $arrPost = array();
            $arrPost['intLibraryType'] = $tipe_library;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function InsertTopUpDeposit($post){
            $sp_name = "LibDisp_Internal_TopUpDepositFromBackend";
            $arrPost = array();
            $arrPost['intLibraryID'] = $post['intLibraryID'];
            $arrPost['txtCurrency'] = $post['txtCurrency'];
            $arrPost['txtEmail'] = $post['txtEmail'];
            $arrPost['curTopUpDeposit'] = $post['curTopUpDeposit'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function HistoryTopupLibrary($id_library){
            $sp_name = "LibDisp_Internal_GetHistoryTopUpLibrary";
            $arrPost = array();
            $arrPost['intLibraryID'] = $id_library;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function ValidateDepositTransaction($post){
            $sp_name = "LibDisp_Internal_ManualValidationTransaction";
            $arrPost = array();
            $arrPost['intLibraryID'] = $post['intLibraryID'];
            $arrPost['intTrPaymentGatewayID'] = $post['intTrPaymentGatewayID'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
    }
    



?>