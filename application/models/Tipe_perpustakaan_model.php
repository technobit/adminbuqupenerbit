<?php
    class Tipe_Perpustakaan_Model extends MY_Model{
        #code
        function __construct(){
            
        }
        
        function ListTypePerpustakaan(){
            $sp_name = "LibDisp_Internal_LibraryTypeRetrieve";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function DeleteType($id_type){
            $sp_name = "LibDisp_Internal_LibraryTypeDelete";
            $arrPost = array();
            $arrPost['intLibraryType'] = $id_type;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function DetailType($id_type){
            $sp_name = "LibDisp_Internal_LibraryTypeDetail";
            $arrPost = array();
            $arrPost['intLibraryType'] = $id_type;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function insertType($text_type){
            $sp_name = "LibDisp_Internal_LibraryTypeInsert";
            $arrPost = array();
            $arrPost['txtLibraryType'] = $text_type;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function updateType($post){
            $sp_name = "LibDisp_Internal_LibraryTypeUpdate";
            $arrPost = array();
            $arrPost['intLibraryType'] = $post['id-tipe-kategori'];
            $arrPost['txtLibraryType'] = $post['tipe-kategori'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
    }
?>