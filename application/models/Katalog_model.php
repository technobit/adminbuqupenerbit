<?php

class Katalog_Model extends MY_Model{
        #code
        function __construct(){
            
        }
        
        function DetailKatalogLibrary($id){
            $sp_name = "LibDisp_Internal_LibraryCatalogAndCategoryList";
            $arrPost = array();
            $arrPost['intLibraryID'] = $id;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function BookRestrictionCategory($intLibraryID , $intLibraryCategoryID){
            $sp_name = "LibDisp_Internal_CategoryBookRestricionList";
            $arrPost = array();
            $arrPost['intLibraryID'] = $intLibraryID;
            $arrPost['intLibraryCategoryID'] = $intLibraryCategoryID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function BookRestrictionList($intLibraryID , $txtPublisherServicesID, $txtBookID){
            $sp_name = "LibDisp_Internal_BookRestricionList";
            $arrPost = array();
            $arrPost['intLibraryID'] = $intLibraryID;
            $arrPost['txtPublisherServicesID'] = $txtPublisherServicesID;
            $arrPost['txtBookID'] = $txtBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function ListBukuByKatalog($intLibraryID, $intCatalogID , $intLibraryCategoryID){
            $sp_name = "LibDisp_Internal_ListBookOnCategoryInLibrary";
            $service_name = "CallSpGetBookList";
            $arrPost = array();
            $arrPost['intLibraryID'] = $intLibraryID;
            $arrPost['intLibraryCatalogID'] = $intCatalogID;
            $arrPost['intLibraryCategoryID'] = $intLibraryCategoryID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , $service_name);    
            return $retVal;
        }
        
        function DetailBukuByID($txtPublisherServicesID, $txtBookID){
            $sp_name = "LibDisp_Internal_DetailBook";
            $service_name = "CallSpGetBookDetail";
            $arrPost = array();
            $arrPost['txtPublisherServicesID'] = $txtPublisherServicesID;
            $arrPost['txtBookID'] = $txtBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , $service_name);    
            return $retVal;
        }
        
        function HistoryLicenseBook($post){
            $sp_name = "LibDisp_Internal_BuyLicensiBookHistory";
            $arrPost = array();
            $arrPost['intPageNumber'] = $post['intPageNumber'];
            $arrPost['intRowsPerPage'] = $post['intRowsPerPage'];
            $arrPost['intLibraryID'] = $post['intLibraryID'];
            $arrPost['txtPublisherServicesID'] = $post['txtPublisherServicesID'];
            $arrPost['txtBookID'] = $post['txtBookID'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
}
?>