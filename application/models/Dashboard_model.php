<?php

    class Dashboard_model extends MY_Model{
        #code
        
        function __construct(){
            
        }

        public function getBukuDiajukan($intPublisherID){
            $sp_name = "PublisherDisp_DashboardBukuDiajukan";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intBukuDiajukan'];
            return $retVal;
        }

        public function getBukuSedangDiReview($intPublisherID){
            $sp_name = "PublisherDisp_DashboardBukuSedangDireview";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intBukuSedangReview'];
            return $retVal;
        }

        public function getBukuTakLolosSeleksi($intPublisherID){
            $sp_name = "PublisherDisp_DashboardBukuTidakLolosSeleksi";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            ///echopre($retVal);
            $retVal = $retVal[0]['intBukuTidakLolosSeleksi'];
            return $retVal;
        }

        public function getBukuSedangDiKonversi($intPublisherID){
            $sp_name = "PublisherDisp_DashboardBukuSedangDiKonversi";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            ///echopre($retVal);
            $retVal = $retVal[0]['intBukuSedangDiKonversi'];
            return $retVal;
        }

        public function getBukuSiapDiReview($intPublisherID){
            $sp_name = "PublisherDisp_DashboardBukuSiapDireview";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intBukuSiapDiReview'];
            //echopre($retVal);
            return $retVal;
        }

        public function getBukuLolosSeleksi($intPublisherID){
            $sp_name = "PublisherDisp_DashboardBukuLolosSeleksi";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intBukuLolosSeleksi'];
            ///echopre($retVal);
            return $retVal;
        }

        public function getBukuSiapDiKonversi($intPublisherID){
            $sp_name = "PublisherDisp_DashboardBukuSiapDiKonversi";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intBukuSiapDiKonversi'];
            //echopre($retVal);
            return $retVal;
        }

        public function getBukuSiapDiBeli($intPublisherID){
            $sp_name = "PublisherDisp_DashboardBukuSiapDiBeli";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            $retVal = $retVal[0]['intBukuSiapDiBeli'];
            ///echopre($retVal);
            return $retVal;
        }
    }