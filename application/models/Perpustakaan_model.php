<?php
    class Perpustakaan_Model extends MY_Model{
        #code
        function __construct(){
            
        }
        
        function ListCountryPerpustakaan(){
            $sp_name = "LibDisp_Internal_ListCountry";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function ListInternalLibrary($post){
            $sp_name = "LibDisp_Internal_ListLibrary";
            $arrPost = array();
            $arrPost['txtLibraryName'] = $post['txtLibraryName'];
            $arrPost['intLibraryType'] = $post['intLibraryType'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function ListInternalLibrarySearch($post){
            $sp_name = "LibDisp_Internal_LibrarySearch";
            $arrPost = array();
            $arrPost['txtDispenserType'] = !empty($post['txtDispenserType']) ? $post['txtDispenserType'] : "";
            $arrPost['intLibraryType'] = $post['intLibraryType'];
            $arrPost['txtLibraryName'] = $post['txtLibraryName'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function DetailInternalLibrary($id){
            $sp_name = "LibDisp_Internal_LibraryDetail";
            $arrPost = array();
            $arrPost['intLibraryID'] = $id;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function ListKategoriKeanggotaan($id){
            $sp_name = "LibDisp_Internal_LibraryGroupRetrieve";
            $arrPost = array();
            $arrPost['intLibraryID'] = $id;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        
        /// Admin Perpustakaan
        function ListAdminPerpustakaan($id){
            $sp_name = "LibDisp_Internal_LibraryAdminRetrieve";
            $arrPost = array();
            $arrPost['intLibraryID'] = $id;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function DetailAdminPerpustakaan($post){
            $sp_name = "LibDisp_Internal_LibraryAdminDetail";
            $arrPost = array();
            $arrPost['intLibraryID'] = $post['intLibraryID'];
            $arrPost['intLibraryStaffID '] = $post['intLibraryStaffID'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function UpdateAdminPerpustakaan($post){
            $sp_name = "LibDisp_Internal_LibraryAdminUpdate";
            $arrPost = array();
            $arrPost['intLibraryID'] = $post['intLibraryID'];
            $arrPost['intLibraryStaffID '] = $post['intLibraryStaffID'];
            $arrPost['txtUserName'] = $post['txtUserName'];
            $arrPost['txtEmail'] = $post['txtEmail'];
            $arrPost['bitActive'] = $post['bitActive'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function InsertAdminPerpustakaan($post){
            $sp_name = "LibDisp_Internal_LibraryAdminInsert";
            $arrPost = array();
            $arrPost['intLibraryID'] = $post['intLibraryID'];
            $arrPost['txtUserName'] = $post['txtUserName'];
            $arrPost['txtEmail'] = $post['txtEmail'];
            $arrPost['bitActive'] = $post['bitActive'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function DeleteAdminPerpustakaan($post){
            $sp_name = "LibDisp_Internal_LibraryAdminDelete";
            $arrPost = array();
            $arrPost['intLibraryID'] = $post['intLibraryID'];
            $arrPost['intLibraryStaffID '] = $post['intLibraryStaffID'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function ResetPassAdminPerpustakaan($post){
            $sp_name = "LibDisp_Internal_AdminLibraryForgetPassword";
            $arrPost = array();
            $arrPost['intLibraryID'] = $post['intLibraryID'];
            $arrPost['txtEmail '] = $post['txtEmail'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        
        //// Toko Buku
        function ListTokoBuku(){
            $sp_name = "LibDisp_Internal_BookStoreList";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function ListTokoBukuInternal($id){
            $sp_name = "LibDisp_Internal_LibraryBookStoreList";
            $arrPost = array();
            $arrPost['intLibraryID'] = $id;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function AddTokoBukuInternal($post){
            $sp_name = "LibDisp_Internal_BookStoreLibraryInsert";
            $arrPost = array();
            $arrPost['intLibraryID'] = $post['intLibraryID'];
            $arrPost['txtBookStoreServicesID'] = $post['txtBookStoreServicesID'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function DeleteTokoBukuInternal($post){
            $sp_name = "LibDisp_Internal_LibraryBookStoreDelete";
            $arrPost = array();
            $arrPost['intLibraryID'] = $post['intLibraryID'];
            $arrPost['txtBookStoreServicesID'] = $post['txtBookStoreServicesID'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        
        //// perpustakaan
        function getDetailPerpustakaan($id){
            $sp_name = "LibDisp_Internal_LibraryDetail";
            $arrPost = array();
            $arrPost['intLibraryID'] = $id;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter);    
            return $retVal;
        }
        
        function InsertLibrary($post){
            $sp_name = "LibDisp_Internal_LibraryInsert";
            $arrPost = array();
            $arrPost['intLibraryType'] = !empty($post['intLibraryType']) ? $post['intLibraryType'] : 0;
            $arrPost['txtDispenserType'] = $post['txtDispenserType'];
            $arrPost['txtLibraryName'] = $post['txtLibraryName'];
            $arrPost['txtLibraryAddress'] = $post['txtLibraryAddress'];
            $arrPost['txtLibraryEmail'] = $post['txtLibraryEmail'];
            $arrPost['txtCountry'] = $post['txtCountry'];
            $arrPost['txtLibraryPhone'] = $post['txtLibraryPhone'];
            $arrPost['intMaxBorrowPeriod'] = $post['intMaxBorrowPeriod'];
            $arrPost['intUserMaxBorrow'] = $post['intUserMaxBorrow'];
            $arrPost['intUserMaxBorrowMonthly'] = $post['intUserMaxBorrowMonthly'];
            $arrPost['intUserMaxBorrowSequence'] = $post['intUserMaxBorrowSequence'];
            $arrPost['intUserCanBorrowAfter'] = $post['intUserCanBorrowAfter'];
            $arrPost['bitActiveStatus'] = $post['bitActiveStatus'];
            $arrPost['bitRevShare'] = $post['bitRevShare'];
            $arrPost['decRevBuqu'] = !empty($post['decRevBuqu']) ? $post['decRevBuqu'] : 0;
            $arrPost['decRevPartner1'] = !empty($post['decRevPartner1']) ? $post['decRevPartner1'] : 0;
            $arrPost['decRevPartner2'] = !empty($post['decRevPartner1']) ? $post['decRevPartner1'] : 0;
            $arrPost['txtPartnerName1'] = $post['txtPartnerName1'];
            $arrPost['txtPartnerName2'] = $post['txtPartnerName2'];
            $arrPost['intTokenPeriod'] = !empty($post['intTokenPeriod']) ? $post['intTokenPeriod'] : 0;
            $arrPost['curBuy1Token'] = !empty($post['curBuy1Token']) ? $post['curBuy1Token'] : 0;
            $arrPost['curBuy2Token'] = !empty($post['curBuy2Token']) ? $post['curBuy2Token'] : 0;
            $arrPost['curBuy3Token'] = !empty($post['curBuy3Token']) ? $post['curBuy3Token'] : 0;
            $arrPost['curBuy4Token'] = !empty($post['curBuy4Token']) ? $post['curBuy4Token'] : 0;
            $arrPost['curBuy5Token'] = !empty($post['curBuy5Token']) ? $post['curBuy5Token'] : 0;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            
            $retVal = $this->retrieveData($retParameter);
            $retVal['parameter_sp'] = $retParameter;
            return $retVal;
        }
        
        function UpdateLibrary($post){
            $sp_name = "LibDisp_Internal_LibraryUpdate";
            $arrPost = array();
            $arrPost['intLibraryID'] = !empty($post['intLibraryID']) ? $post['intLibraryID'] : 0;
            $arrPost['intLibraryType'] = !empty($post['intLibraryType']) ? $post['intLibraryType'] : 0;
            $arrPost['txtDispenserType'] = $post['txtDispenserType'];
            $arrPost['txtLibraryName'] = $post['txtLibraryName'];
            $arrPost['txtLibraryAddress'] = $post['txtLibraryAddress'];
            $arrPost['txtLibraryEmail'] = $post['txtLibraryEmail'];
            $arrPost['txtCountry'] = $post['txtCountry'];
            $arrPost['txtLibraryPhone'] = $post['txtLibraryPhone'];
            $arrPost['intMaxBorrowPeriod'] = $post['intMaxBorrowPeriod'];
            $arrPost['intUserMaxBorrow'] = $post['intUserMaxBorrow'];
            $arrPost['intUserMaxBorrowMonthly'] = $post['intUserMaxBorrowMonthly'];
            $arrPost['intUserMaxBorrowSequence'] = $post['intUserMaxBorrowSequence'];
            $arrPost['intUserCanBorrowAfter'] = $post['intUserCanBorrowAfter'];
            $arrPost['bitActiveStatus'] = $post['bitActiveStatus'];
            $arrPost['bitRevShare'] = $post['bitRevShare'];
            $arrPost['decRevBuqu'] = !empty($post['decRevBuqu']) ? $post['decRevBuqu'] : 0;
            $arrPost['decRevPartner1'] = !empty($post['decRevPartner1']) ? $post['decRevPartner1'] : 0;
            $arrPost['decRevPartner2'] = !empty($post['decRevPartner2']) ? $post['decRevPartner2'] : 0;
            $arrPost['txtPartnerName1'] = $post['txtPartnerName1'];
            $arrPost['txtPartnerName2'] = $post['txtPartnerName2'];
            $arrPost['intTokenPeriod'] = !empty($post['intTokenPeriod']) ? $post['intTokenPeriod'] : 0;
            $arrPost['curBuy1Token'] = !empty($post['curBuy1Token']) ? $post['curBuy1Token'] : 0;
            $arrPost['curBuy2Token'] = !empty($post['curBuy2Token']) ? $post['curBuy2Token'] : 0;
            $arrPost['curBuy3Token'] = !empty($post['curBuy3Token']) ? $post['curBuy3Token'] : 0;
            $arrPost['curBuy4Token'] = !empty($post['curBuy4Token']) ? $post['curBuy4Token'] : 0;
            $arrPost['curBuy5Token'] = !empty($post['curBuy5Token']) ? $post['curBuy5Token'] : 0;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            
            $retVal = $this->retrieveData($retParameter);
            $retVal['parameter_sp'] = $retParameter;
            return $retVal;
        }
    }
?>