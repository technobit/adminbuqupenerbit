<?php

    class User_Model extends MY_Model{
        #code
        
        function __construct(){
            
        }
        
        function get_user_login($username,$password ){
            $sp_name = "PublisherDisp_Login";
            $arrPost['txtEmail'] = $username;
            $arrPost['txtPassword'] = $password;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");    
            return $retVal;
        }
        
        function get_forget_password($email , $publisher){
            $sp_name = "PublisherDisp_ForgetPassword";
            $arrPost['txtPublisherName'] = $publisher;
            $arrPost['txtEmail'] = $email;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getAdminList($intPublisherID){
            $sp_name = "PublisherDisp_AdminList";
            $arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function nonactiveUser($intPublisherID,$intPublisherUserID){
            $sp_name = "PublisherDisp_UpdateAdminStatusNonAktif";
            $arrPost['intPublisherID'] = $intPublisherID;
            $arrPost['intPublisherUserID'] = $intPublisherUserID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function activeUser($intPublisherID,$intPublisherUserID){
            $sp_name = "PublisherDisp_UpdateAdminStatusAktif";
            $arrPost['intPublisherID'] = $intPublisherID;
            $arrPost['intPublisherUserID'] = $intPublisherUserID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function addNewUser($intPublisherID,$addTxtUserName,$addTxtEmail,$addTxtPassword){
            $sp_name = "PublisherDisp_UserAdminInsert";
            $arrPost['intPublisherID'] = $intPublisherID;
            $arrPost['txtUserName'] = $addTxtUserName;
            $arrPost['txtEmail'] = $addTxtEmail;
            $arrPost['txtPassword'] = $addTxtPassword;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function removeUser($intPublisherID,$intPublisherUserID){
            $sp_name = "PublisherDisp_AdminDelete";
            $arrPost['intPublisherID'] = $intPublisherID;
            $arrPost['intPublisherUserID'] = $intPublisherUserID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function editUser($intPublisherID,$intPublisherUserID,$txtUserName,$txtEmail){
            $sp_name = "PublisherDisp_UpdateAdminInfo";
            $arrPost['intPublisherID'] = $intPublisherID;
            $arrPost['intPublisherUserID'] = $intPublisherUserID;
            $arrPost['txtUserName'] = $txtUserName;
            $arrPost['txtEmail'] = $txtEmail;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function editPass($intPublisherID,$intPublisherUserID,$txtEmail,$txtOldPassword,$txtNewPassword){
            $sp_name = "PublisherDisp_ChangeAdminPassword";
            $arrPost['intPublisherID'] = $intPublisherID;
            $arrPost['intPublisherUserID'] = $intPublisherUserID;
            $arrPost['txtEmail'] = $txtEmail;
            $arrPost['txtOldPassword'] = $txtOldPassword;
            $arrPost['txtNewPassword'] = $txtNewPassword;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
    }
?>