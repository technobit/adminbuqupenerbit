<?php

    class Penerbit_model extends MY_Model{
        #code
        
        function __construct(){
            
        }
        
        function registrationPubliser($post){
            $sp_name = "PublisherDisp_Registration";
            $arrPost['txtPublisherGroup'] = $post['txtPublisherGroup'];
            $arrPost['txtPublisherName'] = $post['txtPublisherName'];
            $arrPost['txtCountry'] = $post['txtCountry'];
            $arrPost['txtProvince'] = $post['txtProvince'];
            $arrPost['txtCity'] = $post['txtCity'];
            $arrPost['txtAddress'] = $post['txtAddress'];
            $arrPost['txtPostalCode'] = $post['txtPostalCode'];
            $arrPost['txtEmail'] = $post['txtEmail'];
            $arrPost['txtPhoneNumber'] = $post['txtPhoneNumber'];
            $arrPost['txtFax'] = $post['txtFax'];
            $arrPost['txtURLWebsite'] = $post['txtFax'];
            $arrPost['bitAPPTI'] = in_array('bitAPPTI',$post['txtAnggota']) ? 1 : 0;
            $arrPost['bitIKAPI'] = in_array('bitIKAPI',$post['txtAnggota']) ? 1 : 0;
            $arrPost['bitOther'] = in_array('bitOther',$post['txtAnggota']) ? 1 : 0;
            $arrPost['txtUserName'] = $post['txtUserName'];
            $arrPost['txtEmailAdmin'] = $post['txtUserName'];
            $arrPost['txtPassword'] = $post['txtPassword'];
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");    
            return $retVal;
        }
        
        function detailPenerbit($publisherId){
            $sp_name = "PublisherDisp_PublihserProfileRetrieve";
            $arrPost['intPublisherID'] = $publisherId;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        
        function updatePenerbit($post){
            $sp_name = "PublisherDisp_UpdateProfile";
            $arrPost['intPublisherID'] = $post['intPublisherID'];
            $arrPost['txtPublisherGroup'] = $post['txtPublisherGroup'];
            $arrPost['txtPublisherName'] = $post['txtPublisherName'];
            $arrPost['txtCountry'] = $post['txtCountry'];
            $arrPost['txtProvince'] = $post['txtProvince'];
            $arrPost['txtCity'] = $post['txtCity'];
            $arrPost['txtAddress'] = $post['txtAddress'];
            $arrPost['txtPostalCode'] = $post['txtPostalCode'];
            $arrPost['txtEmail'] = $post['txtEmail'];
            $arrPost['txtPhoneNumber'] = $post['txtPhoneNumber'];
            $arrPost['txtFax'] = $post['txtFax'];
            $arrPost['txtURLWebsite'] = $post['txtURLWebsite'];
            $arrPost['txtPhoneNumber1'] = $post['txtPhoneNumber1'];
            $arrPost['txtContact1'] = $post['txtContact1'];
            $arrPost['txtEmail1'] = $post['txtEmail1'];
            $arrPost['txtPhoneNumber2'] = $post['txtPhoneNumber2'];
            $arrPost['txtContact2'] = $post['txtContact2'];
            $arrPost['txtEmail2'] = $post['txtEmail2'];
            
            $arrPost['txtBankName'] = $post['txtBankName'];
            $arrPost['txtBankAccount'] = $post['txtBankAccount'];
            $arrPost['txtBankUserName'] = $post['txtBankUserName'];
            $arrPost['txtBankNameOps'] = $post['txtBankNameOps'];
            $arrPost['txtNoBankAccountOps'] = $post['txtNoBankAccountOps'];
            $arrPost['txtBankUserNameOps'] = $post['txtBankUserNameOps'];
            
            $arrPost['bitAPPTI'] = in_array('bitAPPTI',$post['txtAnggota']) ? 1 : 0;
            $arrPost['bitIKAPI'] = in_array('bitIKAPI',$post['txtAnggota']) ? 1 : 0;
            $arrPost['bitOther'] = in_array('bitOther',$post['txtAnggota']) ? 1 : 0;
            
            $arrPost['txtContactOwner'] = $post['txtContactOwner'];
            $arrPost['txtPhoneNumberOwner'] = $post['txtPhoneNumberOwner'];
            $arrPost['txtEmailOwner'] = $post['txtEmailOwner'];
            $arrPost['txtNPWP'] = $post['txtNPWP'];

            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");    
            return $retVal;
        }
        
        function getListBank(){
            $sp_name = "PublisherDisp_ListOfBank";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        
    }
    



?>
