<?php

    class Invoice_model extends MY_Model{
        #code
        
        function __construct(){
            
        }
        
        function getListInvoice($intPublisherID){
            $sp_name = "PublisherDisp_PublisherInvoiceList";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getDetailInvoice($intPublisherID, $intYear, $intMonth){
            $sp_name = "PublisherDisp_PublisherInvoiceDetail";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intYear'] = $intYear;
			$arrPost['intMonth'] = $intMonth;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getDetailInvoicePurchase($intPublisherID, $intYear, $intMonth){
            $sp_name = "PublisherDisp_InvoicePurchaseDetail";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intYear'] = $intYear;
			$arrPost['intMonth'] = $intMonth;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function requestInvoice($intPublisherID,$txtInvoiceNumber,$intCopyLicense,$curTotalPurchase,$ntxtInvoiceProof,$intYear,$intMonth){
            $sp_name = "PublisherDisp_RegisterInvoice";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['txtInvoiceNumber'] = $txtInvoiceNumber;
			$arrPost['intCopyLicense'] = $intCopyLicense;
			$arrPost['curTotalPurchase'] = $curTotalPurchase;
			$arrPost['ntxtInvoiceProof'] = $ntxtInvoiceProof;
			$arrPost['intYear'] = $intYear;
			$arrPost['intMonth'] = $intMonth;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
	}
?> 
