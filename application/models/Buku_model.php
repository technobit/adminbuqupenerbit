<?php

    class Buku_model extends MY_Model{
        #code
        
        function __construct(){
            
        }
        
        function getListCatalog(){
            $sp_name = "PublisherDisp_CatalogRetrieve";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function getListCategory($intCatalogID){
            $sp_name = "PublisherDisp_CategoryRetrieve";
            $arrPost = array();
			$arrPost['intCatalogID'] = $intCatalogID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function getListBookStatus(){
            $sp_name = "PublisherDisp_BookStatusRetrieve";
            $arrPost = array();
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function getBookList_old($arrPost){
            $sp_name = "PublisherDisp_BookRegistrationList";

            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intCatalogID'] = $intCatalogID;
			$arrPost['intCategoryID'] = $intCategoryID;
			$arrPost['intStatusID'] = $intStatusID;
            $arrPost['intStatusID'] = $intStatusID;
			$arrPost['txtBookTitle'] = $txtBookTitle;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getBookList($arrPost = array()){
            $sp_name = "PublisherDisp_BookRegistrationList";
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }
        function getBookDetail($intPublisherID,$intPublisherBookID){
            $sp_name = "PublisherDisp_PublihserBookDetail";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function BookRegistration($arrPost){
            $sp_name = "PublisherDisp_BookRegistration";
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function updateBooks($arrPost){
            $sp_name = "PublisherDisp_PublisherBookUpdate";
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function getBookStatus($intPublisherID , $intPublisherBookID){
            $sp_name = "PublisherDisp_BookDetailStatusAndInfo";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function checkPartialUploadPDF($intPublisherID , $intPublisherBookID){
            $sp_name = "PublisherDisp_ValidationBookParsialUpload";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function checkCompleteUploadPDF($intPublisherID , $intPublisherBookID){
            $sp_name = "PublisherDisp_ValidationBookCompleteUpload";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function updatePartialUploadPDF($intPublisherID , $intPublisherBookID , $txtFileNamePDF){
            $sp_name = "PublisherDisp_UpdateBookPDFParsialName";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $arrPost['txtPDFParsialName'] = $txtFileNamePDF;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function updateCompleteUploadPDF($intPublisherID , $intPublisherBookID , $txtFileNamePDF){
            $sp_name = "PublisherDisp_UpdateBookPDFCompleteName";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $arrPost['txtPDFCompleteName'] = $txtFileNamePDF;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function updateStatusToReview($intPublisherID , $intPublisherBookID){
            $sp_name = "PublisherDisp_UpdateBookStatusToAlreadyReview";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function updateStatusToConvert($intPublisherID , $intPublisherBookID){
            $sp_name = "PublisherDisp_UpdateBookStatusToAlreadyKonversi";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

        function deleteBooks($intPublisherID , $intPublisherBookID){
            $sp_name = "PublisherDisp_DeletePublisherBook";
            $arrPost = array();
			$arrPost['intPublisherID'] = $intPublisherID;
			$arrPost['intPublisherBookID'] = $intPublisherBookID;
            $retParameter = $this->soap_library->set_parameter($sp_name , $arrPost);
            $retVal = $this->retrieveData($retParameter , "CallSpExcecution");
            return $retVal;
        }

	}
?>
