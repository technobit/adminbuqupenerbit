<section class="content-header">
          <h1 class="title">Rekap Sebaran Kategori Buku</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="form-daftar-buku">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Filter</h3>
                        <div class="box-tools pull-right">
                			<button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i> </button>
              			</div>
                    </div>
                    <div class="box-body">
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Katalog</label>
                            <div class="col-sm-4">
                                <select class="form-control" onchange="getCategory()" id="intCatalogID" name="intCatalogID">
                                    <option value="0">-Semua-</option>
                                    <?php
                                    foreach ($txtDaftarKatalog as $key) {
                                        echo '<option value="'.$key['intCatalogID'].'">'.$key['txtCatalog'].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Kategori</label>
                            <div class="col-sm-4" id="optCategory">
                                <select class="form-control" id="intCategoryID" name="intCategoryID">
                                    <option value="0">-Semua-</option>
                                </select>
                            </div>
                		</div>
						<div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
	                            <button class="btn btn-success" type="button" id="viewReport">Tampilkan</button>
                            </div>
                		</div>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <form class="form-horizontal" id="frm-detail-penerbit">
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Daftar Buku</h3>
                    </div>
                    <div class="box-body">
                        <table id="tableSebaranKategori" class="table">
                            <thead>
                                <tr>
                                  <th>Kategori</th>
                                  <th>Status</th>
                                  <th>Jumlah Judul</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>
