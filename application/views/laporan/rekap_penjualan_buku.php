<section class="content-header">
          <h1 class="title">Rekap Penjualan Buku</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="form-date">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Filter</h3>
                        <div class="box-tools pull-right">
                			<button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i> </button>
              			</div>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-3">
                            	<div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" id="date1" class="form-control pull-right" name="date1">
                                </div>
                            </div>
                            <div class="col-sm-1">
                        		<label class="control-label form-label">s.d.</label>
                            </div>
                            <div class="col-sm-3">
                            	<div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" id="date2" class="form-control pull-left" name="date2">
                                </div>
                            </div>
                            <div class="col-sm-1">
	                            <button class="btn btn-success" type="button" onClick="loadPenjualanBuku()">Tampilkan</button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <form class="form-horizontal" id="frm-detail-penerbit">
                <div class="box box-primary">
                    <div class="box-body">
                        <table id="tableBookSell" class="table">
                            <thead>
                                <tr>
                                <th>Tahun</th>
                                <th>Bulan</th>
                                <th>Jumlah DRM Dibeli</th>
                                <th>Sub Total Nilai</th>
                                </tr>
                            </thead>
                            <tbody id="listBookData">
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>
