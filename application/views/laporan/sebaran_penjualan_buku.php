<section class="content-header">
          <h1 class="title">Rekap Sebaran Penjualan Buku</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="form-judul-buku">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Filter</h3>
                        <div class="box-tools pull-right">
                			<button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i> </button>
              			</div>
                    </div>
                    <div class="box-body">
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Judul Buku</label>
                            <div class="col-sm-4">
                            	<input type="text" placeholder="Masukkan Judul Buku" class="form-control" id="txtBookTitle" name="txtBookTitle">
                            </div>
                            <div class="col-sm-2">
	                            <button class="btn btn-success" type="button" onClick="loadSebaranPenjualanBuku()">Tampilkan</button>
                            </div>
                		</div>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <form class="form-horizontal" id="frm-detail-penerbit">
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Daftar Buku</h3>
                    </div>
                    <div class="box-body">
                        <table id="tableBookList" class="table">
                            <thead>
                                <tr>
                                <th>Katalog</th>
                                <th>Kategori</th>
                                <th>Nama Perpustakaan</th>
                                <th>Judul Buku</th>
                                </tr>
                            </thead>
                            <tbody id="listBookData">
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>