<section class="content-header">
          <h1 class="title">Detail Status Buku</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="form-date">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Filter</h3>
                        <div class="box-tools pull-right">
                			<button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i> </button>
              			</div>
                    </div>
                    <div class="box-body">
                            <?=$frmBookStatus?>
                            <div class="col-sm-3 col-md-offset-3">
	                            <button class="btn btn-success" type="button" id="btnShowDataBuku"><i class="fa fa-send"></i>Tampilkan</button>
                            </div>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <form class="form-horizontal" id="frm-detail-penerbit">
                <div class="box box-primary">
                    <div class="box-body" id="box-list-buku">
                        <table id="tableDetailStatus" class="table">
                            <thead>
                                <tr>
                                  <th>Katalog</th>
                                  <th>Kategori</th>
                                  <th>ISBN</th>
                                  <th>Judul Buku</th>
                                  <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>
