<section class="content-header">
          <h1 class="title">Rekap Status Buku</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="frm-detail-penerbit">
                <div class="box box-primary">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <table id="tableBookStatus" class="table">
                            <thead>
                                <tr>
                                  <th>No.</th>
                                  <th>Status Buku</th>
                                  <th>Jumlah Buku</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $no = 1; foreach ($statusBuku as $key) { ?>
                            	<tr>
                                	<td><?=$no?></td>
                                	<td><?=$key['txtStatus']?></td>
                                	<td><?=$key['intBookCount']?></td>
                                </tr>
                            <?php $no++; } ?>
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>
