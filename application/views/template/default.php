<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?=$meta_title?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?=ASSETS_URL?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <?php
      echo $custom_css;
    ?>
    <!-- Theme style -->
    <link href="<?=ASSETS_URL?>dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?=ASSETS_URL?>dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>custom/css/global.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- new post -->
    <!-- date and time picker -->
    <!-- Bootstrap datetime Picker -->
    <script>
      var global_url = '<?=base_url();?>';
    </script>
    
  </head>
  <body class="skin-green fixed">
    <div class="wrapper">
    <?=$top_navbar?>
    <?=$side_navbar?>
      <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?=$main_content?>
  </div><!-- /.content-wrapper -->
  <?=$main_footer?>
    </div><!-- ./wrapper -->
    <!-- jQuery 2.1.3 -->
<script src="<?=ASSETS_URL?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?=ASSETS_URL?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Slimscroll -->
<script src="<?=ASSETS_URL?>plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- FastClick -->
<script src='<?=ASSETS_URL?>plugins/fastclick/fastclick.min.js'></script>
<!-- DataTables -->
<script src='<?=ASSETS_URL?>plugins/datatables/jquery.dataTables.min.js'></script>
<script src='<?=ASSETS_URL?>plugins/datatables/dataTables.bootstrap.min.js'></script>
<script src='<?=ASSETS_URL?>plugins/bootbox/bootbox.js'></script>
<!-- AdminLTE App -->
<script src="<?=ASSETS_URL?>dist/js/app.min.js" type="text/javascript"></script>
<!-- Globak App -->
<script src="<?=ASSETS_JS_URL?>global.js" type="text/javascript"></script>
<?=$custom_js?>
  </body>
</html>