<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <!--<li class="header">MAIN MENU</li>-->
      <?php
      foreach($sidebar_nav as $index  => $val):
      ?>
      <?php if(isset($val['sub_menu']) && ($val['sub_menu']=="")): ?>
      <li><a href="<?=base_url().$val['main_url']?>"><span class="<?=$val['class']?>"></span> <?=$val['desc']?></a></li>
      <?php
      else :
      ?>
      <li class="treeview">
        <a href="#">
          <span class="<?=$val['class']?>"></span>  <?=$val['desc']?>
        </a>
        <ul class="treeview-menu">
          <?php
            if(isset($submenu_side[$val["sub_menu"]])) :
            $subMenu = $submenu_side[$val["sub_menu"]];
            foreach($subMenu as $indexSub => $row) :
          ?>
          <li><a href="<?=base_url().$val['main_url'].$row["url"]?>"><span class="<?=$row["class"]?>"></span> <?=$row["desc"]?></a></li>
          <?php
        endforeach;
        endif;
        ?>
        </ul>
      </li>
      <?php
      endif; ?>
      <?php
      endforeach;
      ?>
    </ul>
    <!-- Sidebar user panel -->
      <div class"supporterIcon" style="margin:auto;text-align:center;position:absolute;bottom:10px;left:15px">
      		<img class="logoLuar" src="<?=ASSETS_URL?>img/LogoDalam/APPTI_White.png" alt="APPTI" width="65">
      		<img class="logoLuar" src="<?=ASSETS_URL?>img/LogoDalam/IKAPI_White.png" alt="IKAPI" width="65">
      		<img class="logoLuar" src="<?=ASSETS_URL?>img/LogoDalam/Buqu_White.png" alt="Buqu" width="65">
      </div>
  </section>
  <!-- /.sidebar -->
</aside>