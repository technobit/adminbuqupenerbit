<section class="content-header">
          <h1 class="title">Profil Penerbit</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="frm-detail-penerbit">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Informasi Penerbit</h3>
                    </div>
                    <div class="box-body">
                            <?=$intPublisherID?>
                            <?=$txtPublisherGroup?>
                            <?=$txtPublisherName?>
                            <?=$txtAddress?>
                            <?=$txtCountry?>
                            <?=$txtProvince?>
                            <?=$txtCity?>
                            <?=$txtPostalCode?>
                            <?=$txtPhoneNumber?>
                            <?=$txtFax?>
                            <?=$txtEmail?>
                            <?=$txtURLWebsite?>
                            <?=$txtAnggota?>
                            
                        
                    </div>
                </div>
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Kontak Pimpinan</h3>
                    </div>
                    <div class="box-body">
                            <?=$txtNPWP?>
                            <?=$txtContactOwner?>
                            <?=$txtPhoneNumberOwner?>
                            <?=$txtEmailOwner?>
                    </div>
                </div>

                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Kontak 1</h3>
                    </div>
                    <div class="box-body">
                            <?=$txtContact1?>
                            <?=$txtPhoneNumber1?>
                            <?=$txtEmail1?>
                    </div>
                </div>

                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Kontak 2</h3>
                    </div>
                    <div class="box-body">
                            <?=$txtContact2?>
                            <?=$txtPhoneNumber2?>
                            <?=$txtEmail2?>
                    </div>
                </div>

                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Bank Rekomendasi</h3>
                    </div>
                    <div class="box-body">
                        <?=$txtBankName?>
                        <?=$txtBankAccount?>
                        <?=$txtBankUserName?>
                    </div>
                </div>
                <div class="box box-default">
                    <div class="box-header">
                        <h3 class="box-title">Bank Lainnya</h3>
                    </div>
                    <div class="box-body">
                        <?=$txtBankNameOps?>
                        <?=$txtNoBankAccountOps?>
                        <?=$txtBankUserNameOps?>
                        <div id="result-update-container">
                            
                        </div>
                    </div>
                </div>

                        <div class="col-md-12 text-center">
                                <button class="btn btn-primary btn-flat" id="btnUpdatePenerbit"><i class="fa fa-send"></i> Simpan</button>
                        </div>
                </form>
            </div>
        </div>
</section>
