<section class="content-header">
          <h1 class="title"><?=$title?></h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <?php 
            if($mode=="update") : 
            ?>
            <div class="col-md-12">
                <div class="box box-primary" id="box-detail-buku">
                    <div class="box-header">
                        <h3 class="box-title">Informasi</h3>
                    </div>
                    <div class="box-body">
                    <div class="col-md-8">
                    <dl class="dl-horizontal">
                        <dt>Status</dt>
                        <dd><?=$booksDetailInformation['txtStatus']?></dd>
                        <dt>Total DRM Terjual</dt>
                        <dd><?=$booksDetailInformation['curBookPrice']?></dd>
                    </dl>
                    </div>
                    <div class="col-md-4">
                    <?php
                    if(!empty($imageUrlFront)) :  
                    ?>
                    <img src="<?=$imageUrlFront?>" class="img-responsive pull-right" height="100" width="100" />
                    <?php 
                    endif;
                    ?>
                    </div>
                    </div>
                </div>
            </div>
            <?php 
            endif; 
            ?>
            <div class="col-md-12">
                <form action="<?=$link_post_form?>" class="form-horizontal" id="form-tambah-buku" enctype="multipart/form-data" method="POST">
                <div class="box box-success" id="box-form-tambah-buku">
                    <div class="box-header">
                        <h3 class="box-title">Form Data Buku</h3>
                    </div>
                    <div class="box-body">
                    	<div class="col-sm-12">
                            <?=$intPublisherID?>
                            <?=$intPublisherBookID?>
                            <?=$txtInternalBookID?>
                            <?=$txtBookTitle?>
                            <?=$txtSubBookTitle?>
                            <?=$txtLanguage?>
                            <?=$txtPublisherName?>
                            <?=$txtAuthor?>
                            <?=$txtImprint?>
                            <?=$txtYear?>
                            <?=$txtEdition?>
                            <?=$intBookPage?>
                            <?=$txtISBN?>
                            <?=$intBookRentDays?>
                            <?=$bitAvailableOnline?>
                            <?=$curBookPriceOnline?>
                            <?=$bitAvailableOffline?>
                            <?=$curBookPriceOffline?>
                        </div>
                    </div>
                </div>
                <div class="box box-primary" id="box-form-kata-kunci">
                    <div class="box-header">
                        <h3 class="box-title">Deskripsi Buku</h3>
                    </div>
                    <div class="box-body">
                        <?=$txtShortDescription?>
                        <?=$txtSynopsis?>
                        <?=$txtKeyWords?>
                    </div>
                </div>
                <div class="box box-warning" id="box-form-kategori">
                    <div class="box-header">
                        <h3 class="box-title">Kategori Buku</h3>
                    </div>
                    <div class="box-body">
                        <?=$intCatalogID1?>
                        <?=$intCategoryID1?>
                        <?=$intCatalogID2?>
                        <?=$intCategoryID2?>
                        <?=$intCatalogID3?>
                        <?=$intCategoryID3?>
                    </div>
                </div>

                <div class="box box-danger" id="box-form-file">
                    <div class="box-header">
                        <h3 class="box-title">Lampiran Sampul Buku</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group" id="frontCover">
                                <label class="col-sm-3 control-label form-label">Sampul Depan</label>
                                <div class="col-sm-9">
                                    <?=$ntxtFrontCover?>
                                    <div class="input-group">
                                        <?=form_input("imgFrontCover" , "","class='form-control' id='imgFrontCover' readonly = 'true'")?>
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-flat" type="button" id="btnImgFrontCover"><i class="fa fa-upload"></i> Upload</button>
                                        </span>
                                        
                                    </div>
                                    <span class="help-block text-red">* Ukuran Resolusi Gambar Minimal 300x300</span>
                                    <input type="file" id="ntxtFrontCover" name="ntxtFrontCover" style="display:none;">
                                    <?=$txtFrontCoverFileName?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label form-label">Sampul Belakang</label>
                                <div class="col-sm-9">
                                    <?=$ntxtBackCover?>
                                    <div class="input-group">
                                        <?=form_input("imgBackCover" , "","class='form-control' id='imgBackCover' readonly = 'true'")?>
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-flat" type="button" id="btnImgBackCover"><i class="fa fa-upload"></i> Upload</button>
                                        </span>
                                        
                                    </div>
                                    <span class="help-block text-red">* Ukuran Resolusi Gambar Minimal 300x300</span>
                                    <input type="file" id="ntxtBackCover" name="ntxtBackCover" style="display:none;">
                                    <?=$txtBackCoverFileName?>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="text-center">
                    <?=$frmMode?>

                    <button class="btn btn-success" onclick="saveDataProses()" id="btnDaftar" type="submit"><i class="fa fa-save"></i> <?=$btnUpdate?></button>
                    <?php 
                        if($mode=="update") : 
                            if($statusBuku==2) { 
                    ?>
                            <button onclick="updateFiling('review')" class="btn btn-warning" id="btnFilling" type="button"><i class="fa fa-pencil-square-o"></i> Ajukan Buku Untuk Review</button>
                            <?php 
                            }else if($statusBuku==5) { 
                            ?>
                            <button onclick="updateFiling('konversi')" class="btn btn-warning" id="btnFilling" type="button"><i class="fa fa-pencil-square-o"></i> Ajukan Buku Untuk Konversi</button>
                    <?php 
                            }
                            if(($statusBuku==2) || ($statusBuku==3) || ($statusBuku==5)) :
                    ?>
                            <button onclick="deleteBooksData()" class="btn btn-danger" id="btnFilling" type="button"><i class="fa fa-trash"></i> Hapus</button>
                    <?php 
                            endif;
                        endif;

                    ?>
                </div>
                </form>
            </div>
            <?php 
                if($isUploadPDF==true) : 
            ?>
            <div class="col-sm-12" style="margin-top:20px;">
                <div class="box box-success" id="box-upload-pdf">
                    <div class="box-header">
                            <h3 class="box-title">Upload PDF</h3>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal" id="frm-buku-upload">
                            <div class="form-group">
                                <label class="col-sm-3 control-label form-label">File PDF Sesuai Dengan Status</label>
                                <div class="col-sm-9">
                                <div class="input-group">
                                <?=form_input("fileNamePDF" , "","class='form-control' id='fileNamePDF' readonly = 'true'")?>
                                        <span class="input-group-btn">
                                            <button class="btn btn-default btn-flat" id="btnUploadFile" type="button"><i class="fa fa-upload"></i> Upload PDF</button>
                                        </span>
                                </div>
                                <input type="file" name="filePDF" id="filePDF" style="display : none;"/>
                                <?=$txtModeFormPDF?>
                                </div>
                            </div>              
                            <div class="col-sm-12" id="file-information" style="display:none;margin-top : 20px;">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <dl>
                            <dt>Nama File Yang Di Upload</dt>
                            <dd id="fileName"></dd>
                            <dt>File Rename</dt>
                            <dd id="fileRename"></dd>
                            <dt>Mode</dt>
                            <dd id="fileMode"></dd>
                            <dt>Status</dt>
                            <dd id="fileStatus"></dd>
                        </dl>
                        </div>
                        <div class="col-md-12" style="margin-top : 10px;">
                        <div class="progress active" id="progress-container" style="display : none;">
                            <div style="width: 100%" id="progress-bar-volume" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-success progress-bar-striped">
                            <p id="msg-file-uploading">File Sedang Di Unggah</p>
                            <span class="sr-only">20% Complete</span>
                            </div>    
                        </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php 
                endif;
            ?>
        </div>
</section>