<section class="content-header">
          <h1 class="title">Upload Buku <?=$title?></h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal">
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Data Buku</h3>
                    </div>
                    <div class="box-body">
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Judul Buku</label>
                            <div class="col-sm-4">
	                            <input type="text" class="form-control" disabled value="<?=$detail_buku['txtBookTitle']?>">
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Sub Judul Buku</label>
                            <div class="col-sm-4">
	                            <input type="text" class="form-control" disabled="" value="<?=$detail_buku['txtSubBookTitle']?>">
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Penerbit</label>
                            <div class="col-sm-4">
	                            <input type="text" class="form-control" disabled="" value="<?=$detail_buku['txtPublisherName']?>">
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Tahun</label>
                            <div class="col-sm-4">
	                            <input type="text" class="form-control" disabled="" value="<?=$detail_buku['intYear']?>">
                            </div>
                		</div>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <form class="form-horizontal" id="form-daftar-buku">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Form Upload</h3>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal" id="frm-buku-upload">
                        <?=$frm_id_publisher?>
                        <?=$frm_id_buku?>
                        <?=$frm_title_buku?>
                        <?=$frm_mode_form?>
                        <div class="col-sm-12">
                        <dl style="display:none">
                            <dt style="display:">Judul Buku</dt>
                            <dd><?=$detail_buku['txtBookTitle']?></dd>
                            <dt>Sub Judul Buku</dt>
                            <dd><?=$detail_buku['txtSubBookTitle']?></dd>
                            <dt>Penerbit Buku</dt>
                            <dd><?=$detail_buku['txtPublisherName']?></dd>
                            <dt>Tahun Penerbit</dt>
                            <dd><?=$detail_buku['intYear']?></dd>
                        </dl>
                        <input type="file" name="filePDF" id="filePDF" style="display : none;"/>
                        <button class="btn btn-lg btn-default btn-flat" id="btnUploadFile" type="button"><i class="fa fa-upload"></i> Upload PDF</button>
                        <a href="<?=$link_back?>" class="btn btn-lg btn-warning btn-flat" id="btnBack"><i class="fa fa-reply"></i> Kembali</a>
                        </div>
                        <div class="col-sm-12" id="file-information" style="display:none;margin-top : 20px;">
                        <dl>
                            <dt>Nama File Yang Di Upload</dt>
                            <dd id="fileName"></dd>
                            <dt>File Rename</dt>
                            <dd id="fileRename"></dd>
                            <dt>Mode</dt>
                            <dd id="fileMode"></dd>
                            <dt>Status</dt>
                            <dd id="fileStatus"></dd>
                        </dl>
                        </div>
                        <div class="col-md-12" style="margin-top : 10px;">
                        <div class="progress active" id="progress-container" style="display : none;">
                            <div style="width: 100%" id="progress-bar-volume" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar progress-bar-success progress-bar-striped">
                            <p id="msg-file-uploading">File Sedang Di Unggah</p>
                            <span class="sr-only">20% Complete</span>
                            </div>    
                        </div>
                        </div>
                        </form>
                    </div>
                </div>
                </form>
            </div>            
        </div>
</section>

