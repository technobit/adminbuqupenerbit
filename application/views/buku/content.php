<section class="content-header">
          <h1 class="title">Daftar Buku</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="form-daftar-buku">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">Filter</h3>
                        <div class="box-tools pull-right">
                			<button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i> </button>
              			</div>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Status</label>
                            <div class="col-sm-4">
                                <?=$cmbDropdownStatus?>
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Katalog</label>
                            <div class="col-sm-4">
                                <?=$cmbDropdownCatalog?>
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Kategori</label>
                            <div class="col-sm-4" id="optCategory">
                                <?=$cmbDropdownCategory?>
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Judul Buku</label>
                            <div class="col-sm-4">
	                            <input type="text" placeholder="Masukkan Judul Buku" class="form-control" id="txtBookTitle" name="txtBookTitle">
                            </div>
                		</div>
                        <div class="form-group">
                  			<label class="col-sm-2 control-label form-label">ISBN</label>
                            <div class="col-sm-4">
	                            <input type="text" placeholder="Masukkan ISBN" class="form-control" id="txtBookISBN" name="txtBookISBN">
                            </div>
                		</div>
						<div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
	                            <button class="btn btn-success" type="button" id="btnSearchBuku"><i class="fa fa-send"></i> Tampilkan</button>
                            </div>
                		</div>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <form class="form-horizontal" id="frm-detail-penerbit">
                <div class="box box-primary" id="box-list-buku">
                    <div class="box-header">
                    	<h3 class="box-title">Daftar Buku</h3>
                        <div class="box-tools pull-right">
                        <a class="btn btn-primary" type="button" href="<?=base_url()?>buku/tambah/"><i class="fa fa-plus"></i> Registrasi Buku Baru</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="tableBookList" class="table">
                            <thead>
                                <tr>
                                  <th>Cover</th>
                                  <th>Katalog</th>
                                  <th>Kategori</th>
                                  <th>Penerbit</th>
                                  <th>Judul Buku</th>
                                  <th>ISBN</th>
                                  <th>Status</th>
                                  <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="listBookData">
                                
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>
 <?php
      $alert = $this->session->flashdata("alert_save_buku");
      if(isset($alert) && !empty($alert)):
        $message = $alert['message'];
        $status = ($alert['status']==true) ? "Sukses" : "Gagal";
        $class_status = ($alert['status'] == true) ? 'success' : 'danger';
        $icon = ($alert['status'] == true) ? 'check' : 'ban';
    ?>
    <div class="modal modal-<?php echo $class_status ?> fade" id="myModal" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title"><span class="icon fa fa-<?php echo $icon ?>"></span> <?php echo $status?></h4>
          </div>
          <div class="modal-body">
            <p><?php echo $message ?></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline" data-dismiss="modal">OK</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->    
    <?php endif; ?>