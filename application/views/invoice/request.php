<section class="content-header"> 
          <h1 class="title">Pengajuan Invoice</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="formInvoiceRequest">
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Detail Invoice</h3>
                    </div>
                    <div class="box-body">
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">No. Invoice</label>
                            <div class="col-sm-4">
	                            <input type="text" class="form-control" id="txtInvoiceNumber" name="txtInvoiceNumber">
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Tahun</label>
                            <div class="col-sm-4">
	                            <input type="text" class="form-control" disabled="" value="<?=$intYear?>">
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Bulan</label>
                            <div class="col-sm-4">
	                            <input type="text" class="form-control" disabled="" value="<?=date("F", mktime(0, 0, 0, $intMonth));?>">
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Total DRM Terjual</label>
                            <div class="col-sm-4">
	                            <input type="text" class="form-control" disabled="" value="<?=$intCopyLicense?>">
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Total Nilai Penjualan</label>
                            <div class="col-sm-4">
	                            <input type="text" class="form-control" disabled="" value="<?=$curTotalPurchase?>">
                            </div>
                		</div>
						<div class="form-group">
                  			<label class="col-sm-2 control-label form-label">Status</label>
                            <div class="col-sm-4">
	                            <input type="text" class="form-control" disabled="" value="Invoice Belum Diterima">
                            </div>
                		</div>
						<div id="uploadInvoice" class="form-group">
                            <label class="col-sm-2 control-label form-label">Upload Invoice</label>
                            <div class="col-sm-4">
                                <input type="hidden" value="" name="ntxtInvoiceImgTxtDir">
                                <div class="input-group">
                                    <input type="text" readonly id="imgInvoiceImg" class="form-control" value="" name="imgInvoiceImg">
                                    <span class="input-group-btn">
                                        <button id="btnImgInvoiceImg" type="button" class="btn btn-info btn-flat"><i class="fa fa-upload"></i> Upload</button>
                                    </span>
                                </div>
                                <input type="file" style="display:none;" name="ntxtInvoiceImg" id="ntxtInvoiceImg">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
	                            <button onclick="requestInvoice('<?=$intCopyLicense?>','<?=$curTotalPurchase?>','<?=$intYear?>','<?=$intMonth?>')" type="button" class="btn btn-success">Ajukan Invoice</button>
                            </div>
                		</div>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <form class="form-horizontal">
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Detail Buku Terjual</h3>
                    </div>
                    <div class="box-body">
                        <table id="tableBookList" class="table">
                            <thead>
                                <tr>
                                  <th>Tanggal</th>
                                  <th>Nama Perpustakaan</th>
                                  <th>ISBN</th>
                                  <th>Judul Buku</th>
                                  <th>Harga DRM</th>
                                  <th>Jumlah DRM Dibeli</th>
                                  <th>Sub Total Nilai</th>
                                </tr>
                            </thead>
                            <tbody id="detailInvoicePurchase">
                                <?php foreach ($detailInvoicePurchase as $key) {
								?>
                                
                            	<tr>
									<td><?=substr($key['dtBookPurchase'], 0,10)?></td>
									<td><?=$key['txtLibraryName']?></td>
									<td><?=$key['txtISBN']?></td>
									<td><?=$key['txtBookTitle']?></td>
									<td>Rp. <?=$key['curBookLicensePrice']?></td>
									<td><?=$key['intCopyLicense']?></td>
									<td>Rp. <?=$key['curTotalPurchase']?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>


    <!-- Success -->
    <div class="modal modal-success fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Peringatan</h4>
          </div>
          <div class="modal-body" id="alertMsgSuccess">
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

    <!-- Failed -->
    <div class="modal modal-danger fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Peringatan</h4>
          </div>
          <div class="modal-body" id="alertMsgFailed">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default"data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>
