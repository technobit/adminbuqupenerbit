<section class="content-header">
          <h1 class="title">Detail Invoice</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal">
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Detail Invoice</h3>
                    </div>
                    <div class="box-body">
                    	<?php foreach ($detailInvoice as $key) {?>
                        <div class="col-sm-6">    
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">No. Invoice</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$key['txtInvoiceNumber']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Tahun</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$key['intYear']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Bulan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=date("F", mktime(0, 0, 0, $key['intMonth']));?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Total DRM Terjual</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$key['intCopyLicense']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Total Nilai Penjualan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$key['curTotalPurchase']?>">
                                </div>
                            </div>
                            <?php if($key['intStatus'] == 1){
                                        $stat = 'Invoice Belum Diterima';
                                    }else if($key['intStatus'] == 2){
                                        $stat = 'Invoice Sedang Diproses';
                                    }else if($key['intStatus'] == 3){
                                        $stat = 'Pembayaran Sudah Ditransfer';
                                    } ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label form-label">Status Penagihan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" disabled="" value="<?=$stat?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <?php 
                            $ntxtInvoiceProof = $key['ntxtInvoiceProof'];
                                $img = str_replace(";", ";base64,", $ntxtInvoiceProof);?>
                            <img src="data:<?=$img?>" width="100%">
                        </div>
                        <?php } ?>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-md-12">
                <form class="form-horizontal">
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Detail Buku Terjual</h3>
                    </div>
                    <div class="box-body">
                        <table id="tableBookList" class="table">
                            <thead>
                                <tr>
                                  <th>Tanggal</th>
                                  <th>Nama Perpustakaan</th>
                                  <th>ISBN</th>
                                  <th>Judul Buku</th>
                                  <th>Harga DRM</th>
                                  <th>Jumlah DRM Dibeli</th>
                                  <th>Sub Total Nilai</th>
                                </tr>
                            </thead>
                            <tbody id="detailInvoicePurchase">
                                <?php foreach ($detailInvoicePurchase as $key) {
								?>
                                
                            	<tr>
									<td><?=substr($key['dtBookPurchase'], 0,10)?></td>
									<td><?=$key['txtLibraryName']?></td>
									<td><?=$key['txtISBN']?></td>
									<td><?=$key['txtBookTitle']?></td>
									<td>Rp. <?=number_format($key['curBookLicensePrice'] , 0 , '.','.')?></td>
									<td><?=$key['intCopyLicense']?></td>
									<td>Rp. <?=number_format($key['curTotalPurchase'] , 0 , '.' , '.')?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>
