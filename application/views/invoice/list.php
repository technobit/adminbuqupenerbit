<section class="content-header">
          <h1 class="title">Invoice</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal">
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Daftar Invoice</h3>
                    </div>
                    <div class="box-body">
                        <table id="tableBookList" class="table">
                            <thead>
                                <tr>
                                  <th>Tahun</th>
                                  <th>Bulan</th>
                                  <th>Total Terjual</th>
                                  <th>Total Nilai Penjualan</th>
                                  <th>Status Penagihan</th>
                                  <th></th>
                                </tr>
                            </thead>
                            <tbody id="listInvoice">
                                <?php foreach ($listInvoice as $key) {
									if($key['intStatus'] == 1){
										$stat = 'Invoice Belum Diterima';
										$aksi = '<a class="btn btn-flat btn-warning btn-xs" href="'.$url.'request/'.$key['intYear'].'/'.$key['intMonth'].'/'.$key['intCopyLicense'].'/'.$key['curTotalPurchase'].'">Detil Invoice</a>';
									}else if($key['intStatus'] == 2){
										$stat = 'Invoice Sedang Diproses';
										$aksi = '<a class="btn btn-flat btn-primary btn-xs" href="'.$url.'detail/'.$key['intYear'].'/'.$key['intMonth'].'">Detil Invoice</a>';
									}else if($key['intStatus'] == 3){
										$stat = 'Pembayaran Sudah Ditransfer';
										$aksi = '<a class="btn btn-flat btn-primary btn-xs" href="'.$url.'detail/'.$key['intYear'].'/'.$key['intMonth'].'">Detil Invoice</a>';
									}
								?>
                                
                            	<tr>
									<td><?=$key['intYear']?></td>
									<td><?=$key['intMonth']?></td>
									<td><?=$key['intCopyLicense']?></td>
									<td>Rp. <?=number_format($key['curTotalPurchase'],0,'.','.')?></td>
									<td><?=$stat?></td>
									<td><?=$aksi?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>
