<section class="content-header">
          <h1 class="title">User</h1>
          <?=$breadcrumbs?>
</section>
<section class="content">
      <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal">
                <div class="box box-primary">
                    <div class="box-header">
                    	<h3 class="box-title">Daftar Pengguna</h3>
                    </div>
                    <div class="box-body">
                    	<div class="pull-left">
                        	<button class="btn btn-success" type="button" data-toggle="modal" data-target="#tambah">Tambah Pengguna</button>
                        </div>
                        <table id="tableBookList" class="table">
                            <thead>
                                <tr>
                                  <th>Email</th>
                                  <th>Nama</th>
                                  <th>Status</th>
                                  <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="listUser">
                                <?php foreach ($listUser as $key) {
									if($key['bitActive'] == 1){
										$stat = 'Aktif';
										$aksi = '<button class="btn btn-warning btn-xs btn-flat" type="button"  onclick="nonaktif(\''.$key['txtEmail'].'\',\''.$key['intPublisherUserID'].'\')">Nonaktifkan</button>';
									}else{
										$stat = 'Tidak Aktif';
										$aksi = '<button class="btn btn-success btn-xs btn-flat" type="button" onclick="aktif(\''.$key['txtEmail'].'\',\''.$key['intPublisherUserID'].'\')">Aktifkan</button>';
									}
								?>
                                
                            	<tr>
									<td><?=$key['txtEmail']?></td>
									<td><?=$key['txtUserName']?></td>
									<td><?=$stat?></td>
									<td><?=$aksi?>
                                    	<button class="btn btn-primary btn-xs btn-flat" type="button" onclick="ubah('<?=$key['txtEmail']?>','<?=$key['txtUserName']?>','<?=$key['intPublisherUserID']?>')">Ubah</button>
                                    	<button class="btn btn-danger btn-xs btn-flat" type="button" onclick="hapus('<?=$key['txtEmail']?>','<?=$key['intPublisherUserID']?>')">Hapus</button>
                                      <button class="btn btn-info btn-xs btn-flat" type="button" onclick="ubahPassword('<?=$key['txtEmail']?>','<?=$key['txtUserName']?>','<?=$key['intPublisherUserID']?>')">Ubah Password</button></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>  
                    </div>
                </div>
                </form>
            </div>
        </div>
</section>

    <!-- Tambah -->
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Tambah Pengguna</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" id="addUser">
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Username</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" placeholder="Isikan Username" id="addTxtUserName">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">eMail</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" placeholder="Isikan eMail" id="addTxtEmail">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Password</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" placeholder="Isikan Password" id="addTxtPassword">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Ulangi Password</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" placeholder="Ulangi Password" id="addTxtPassword2">
                    </div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" onclick="addUser()">Simpan</button>
            <button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- NonAktif -->
    <div class="modal modal-warning fade" id="nonaktif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Nonaktifkan</h4>
          </div>
          <div class="modal-body">
            <p>Anda yakin akan menonaktifkan User <b id="nonaktifTxtEmail"></b>?</p>
            <p id="nonaktifUserID" style="display:none"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" onclick="nonaktifUser()">Ya</button>
            <button type="button" class="btn btn-success" data-dismiss="modal">Tidak</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Aktif -->
    <div class="modal modal-success fade" id="aktif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Aktifkan</h4>
          </div>
          <div class="modal-body">
            <p>Anda yakin akan mengaktifkan User <b id="aktifTxtEmail"></b>?</p>
            <p id="aktifUserID" style="display:none"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Tidak</button>
            <button type="button" class="btn btn-success" onclick="aktifUser()">Ya</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Ubah -->
    <div class="modal modal-primary fade" id="ubah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Ubah</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Username</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="ubahTxtUserName" name="txtUserName">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">eMail</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" id="ubahTxtEmail"  name="txtEmail">
						            <p style="display:none" id="ubahUserID"></p>
                    </div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
            <button type="button" class="btn btn-success" onclick="editUser()">Simpan</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Hapus -->
    <div class="modal modal-danger fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Hapus</h4>
          </div>
          <div class="modal-body">
            <p>Anda yakin akan menghapus User <b id="hapusTxtEmail"></b>?</p>
            <p id="hapusUserID" style="display:none"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" onclick="removeUser()">Ya</button>
            <button type="button" class="btn btn-success" data-dismiss="modal">Tidak</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Ubah Password -->
    <div class="modal modal-info fade" id="ubahPass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Ubah Password</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Username</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="ubahPassTxtUserName" name="txtUserName" disabled="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">eMail</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" id="ubahPassTxtEmail" name="txtEmail" disabled="">
						            <p style="display:none" id="ubahPassUserID"  name="userID">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Password Lama</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" placeholder="Isikan password lama" id="txtPasswordOld">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Password Baru</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" placeholder="Isikan password Baru" id="txtPasswordNew">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Ulangi Password Baru</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" placeholder="Ulangi password baru" id="txtPasswordNew2">
                    </div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning" data-dismiss="modal">Keluar</button>
            <button type="button" class="btn btn-success" onclick="editPass()">Simpan</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Success -->
    <div class="modal modal-success fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Peringatan</h4>
          </div>
          <div class="modal-body">
            <p id="alertMsgSuccess"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Failed -->
    <div class="modal modal-danger fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Peringatan</h4>
          </div>
          <div class="modal-body">
            <p id="alertMsgFailed"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default"data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>
