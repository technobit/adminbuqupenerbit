<!-- Start Page Header -->
  <section class="content-header">
          <h1>
            Dashboard
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
  </section>
  <!-- End Page Header -->
<section class="content">
  <div class="row">
            <?php 
            foreach ($arrDashboard as $valDashboard) : 
            ?>
            <div class="col-lg-6" >
              <!-- small box -->
              <div class="small-box bg-green" style="min-height: 150px">
                <div class="inner">
                  <h4>Status : <?=$valDashboard['label']?></h4>
                  <h1 style="font-size:65px;text-align:center;font-weight:bold"><?=$valDashboard['data']?></h1>
                </div>
                <div class="icon">
                  <i class="<?=$valDashboard['icon']?>"></i>
                  
                </div>
                <a target="_blank" class="small-box-footer" href="<?=$valDashboard['link'];?>">Detail <i class="fa fa-arrow-circle-right"></i></a>
                
              </div>
            </div><!-- ./col -->
            <?php 
            endforeach;
            ?>
            
            
          </div>
</section>