<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Lupa Password | Layanan Penerbit Pondok Cerdas Warga</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?=ASSETS_URL?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link href="<?=ASSETS_URL?>dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-box-body">
        <div class="login-logo">
            <img class="logoLuar" src="<?=ASSETS_URL?>img/LogoLuar/Buqu.png" alt="Buqu" width="200">
            <h4><b>Website Layanan Dukungan Penerbit</b></h4>
            <!--<br><h5>Version 1.2.3</h5>-->
        </div><!-- /.login-logo -->
        <!--<p class="login-box-msg" id="msg-box">Sign in to start your session</p>-->
         <?php
            if(isset($alert) && !empty($alert)):
              $message = $alert['message'];
              $status = "Peringatan";
              $class_status = ($alert['status'] == true) ? 'success' : 'danger';
              $icon = ($alert['status'] == true) ? 'check' : 'ban';
          ?>
          <div class="alert alert-<?=$class_status;?> alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-<?=$icon?>"></i> <?=$status?>!</h4>
                <?=$message?>
                <?php
                if($alert['status']) :  
                ?>
                <a href="<?=base_url()."login/"?>" class="btn btn-default btn-flat"><i class="fa fa-sign-in"></i> Klik Untuk Kembali Ke Halaman Login</a>
                <?php
                endif;
                ?>
          </div>
          <?php endif; ?>
        <form action="#" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Masukkan Nama Penerbit" name="txtPublisherName" required="true">
            <span class="glyphicon glyphicon-book form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Email" name="txtEmailPublisher" required="true">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="row">  
            <div class="col-xs-12">
              <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-send"></i> Check</button>
              <a href="<?=base_url()?>login" class="btn btn-danger btn-block btn-flat"><i class="fa fa-arrow-left"></i>  Kembali</a>
            </div><!-- /.col -->
          </div>
        </form>
        <div class"supporterIcon" style="margin:15px auto;text-align:center;">
          <h5>Didukung Oleh :</h5>
            <img class="logoLuar" src="<?=ASSETS_URL?>img/LogoLuar/APPTI.png" alt="APPTI" width="40">
            <img class="logoLuar" src="<?=ASSETS_URL?>img/LogoLuar/IKAPI.png" alt="IKAPI" width="40">
            <img class="logoLuar" src="<?=ASSETS_URL?>img/LogoLuar/Buqu.png" alt="Buqu" width="65">
        </div>
      </div><!-- /.login-box-body -->
      
    </div><!-- /.login-box -->
   
    <!-- alert -->
    <?php
      if(isset($alert) && !empty($alert)):
        $message = $alert['message'];
        $status = "Peringatan";
        $class_status = ($alert['status'] == true) ? 'success' : 'danger';
        $icon = ($alert['status'] == true) ? 'check' : 'ban';
    ?>
    <div class="modal modal-<?php echo $class_status ?> fade" id="myModal" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title"><span class="icon fa fa-<?php echo $icon ?>"></span> <?php echo $status?></h4>
          </div>
          <div class="modal-body">
            <p><?php echo $message ?></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline" data-dismiss="modal">OK</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->    
    <?php endif; ?>
    <!-- jQuery 2.1.3 -->
    <script src="<?=ASSETS_URL?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?=ASSETS_URL?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?=ASSETS_URL?>custom/js/login/forget.js"></script>
  </body>
</html>