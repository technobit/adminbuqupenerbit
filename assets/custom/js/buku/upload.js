$(function(){
    $('#btnUploadFile').click(function(){
        $('#filePDF').click();
    });

    $('#filePDF').change(function(){
        checkUploadedFile();
    });
});

function callFilePDF(){
$('#filePDF').click();
}

function uploadFileToTemporary(){
    var loading = getLoadingOverlay('upload-pdf-file');
    $('#box-upload-pdf').append(loading);
    $('#file-information').css("display" , "none");
    var names = $('#filePDF').prop("files")[0].name;
    $('#fileNamePDF').val(names);
    $('#progress-container').css("display" , "block");
    $('#progress-container').addClass('active');
    $('#progress-bar-volume').removeClass('progress-bar-danger');
    $('#progress-bar-volume').addClass('progress-bar-success');
    $('#btnUploadFile').attr("disabled" , "disabled");
    $('#msg-file-uploading').html("File Sedang Di Upload");
    ////$('#btnBack').attr("disabled" , "disabled");
    var file_data = $('#filePDF').prop("files")[0];
    var form_data = new FormData();  
    form_data.append("intBooksPublisherID",$('input[name=intPublisherBookID]').val());
    form_data.append("txtBooksTitle",$('input[name=txtBookTitle]').val());
    form_data.append("txtModeForm" , $('input[name=txtModeFormPDF]').val());
    form_data.append("filePDF", file_data);
    $.ajax({
        url : global_url+"buku/uploadTempFilePDF/",
        type : "POST",
        data : form_data,
        contentType: false,
        processData: false,
        success: function msg(response){
            var data = jQuery.parseJSON(response);
            var status = data['status'];
            var message = data['message'];
            if(status==true){
                $('#progress-container').removeClass('active');
                $('#progress-bar-volume').removeClass('progress-bar-danger');
                $('#progress-bar-volume').addClass('progress-bar-success');
                $('#progress-container').css("display" , "none");
                $('#msg-file-uploading').html(message);
            }else{
                $('#progress-container').removeClass('active');
                $('#progress-bar-volume').removeClass('progress-bar-success');
                $('#progress-bar-volume').addClass('progress-bar-danger');
                $('#progress-container').css("display" , "none");
                $('#msg-file-uploading').html(message);
            }
            resultFileUpload(data);
            $('#btnUploadFile').removeAttr("disabled");
            ///$('#btnBack').removeAttr("disabled");
        }
    });
}

function checkUploadedFile(){
    var txtModeForm = $('input[name=txtModeFormPDF]').val();
    var intPublisherID = $('input[name=intPublisherID]').val();
    var intPublisherBookID = $('input[name=intPublisherBookID]').val();
    var loading = getLoadingOverlay('upload-pdf-file');
    $('#box-upload-pdf').append(loading);
    $.ajax({
        url : global_url+"buku/validasi-upload/",
        type : "POST",
        data : "mode="+txtModeForm+"&intPublisherID="+intPublisherID+"&intBookPublisherID="+intPublisherBookID,
        dataType : "html",
        success : function msg(response){
            var data = jQuery.parseJSON(response);
            var status = data['status'];
            var message = data['message'];
            if(status==true){
                uploadFileToTemporary();
            }else{
                bootbox.confirm({
                    size : "small",
                    title : "Peringatan",
                    message : message,
                    callback : function(result){
                        if(result==true){
                            uploadFileToTemporary(); 
                        }
                    }
                });
            }
            $('#upload-pdf-file').remove();
        }

    });
}

function resultFileUpload(data){
    $('#file-information').css("display" , "block");
    var status = data['status'];
    $('#fileNamePDF').val("");
    if(status==true){
        if($('#file-information').hasClass('alert-danger alert-dismissible')){
            $('#file-information').removeClass('alert-danger alert-dismissible')
        }
        $('#file-information').addClass('alert alert-success alert-dismissible');
    }else{
        if($('#file-information').hasClass('alert-success alert-dismissible')){
            $('#file-information').removeClass('alert-success alert-dismissible')
        }
        $('#file-information').addClass('alert alert-danger alert-dismissible');
    }
    $('#upload-pdf-file').remove();
    $('#fileName').html(data['real_name_file']);
    $('#fileRename').html(data['new_name_file']);
    $('#fileMode').html(data['file_mode']);
    $('#fileStatus').html(data['message']);
    
}