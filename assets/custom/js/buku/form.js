$(function(){
    $('#form-tambah-buku').validate({
        ignore : "",
        rules : {
            txtBookTitle : {
                required : true,
            },ntxtBackCover : {
                accept : "image/*",
            },ntxtFrontCover : {
                accept : "image/*",
            }   
        }
    });
    $('#btnImgFrontCover').click(function(){
        $('#ntxtFrontCover').click();
        
    });
    $('#btnImgBackCover').click(function(){
        $('#ntxtBackCover').click();
        
    });
    $('#ntxtBackCover').change(function(){
        var names = $('#ntxtBackCover').prop("files")[0].name;
        $('#imgBackCover').val(names);
        $('#ntxtBackCover').valid();
        ///uploadFileToConvert($('#ntxtBackCover') , 'ntxtBackCoverDir');
    });
    $('#ntxtFrontCover').change(function(){
        
        var names = $('#ntxtFrontCover').prop("files")[0].name;
        $('#imgFrontCover').val(names);
        $('#ntxtFrontCover').valid();
        
        ///uploadFileToConvert($('#ntxtFrontCover') , 'ntxtFrontCoverTxtDir');
    });

    $('#bitAvailableOnline1').click(function(){
        if($('#bitAvailableOnline1').is(':checked')){
            $('#curBookPriceOnline').removeAttr('readonly');
        }else{
            $('#curBookPriceOnline').attr('readonly' , 'readonly');
        }
    });

    $('#bitAvailableOffline1').click(function(){
        if($('#bitAvailableOffline1').is(':checked')){
            $('#curBookPriceOffline').removeAttr('readonly');
        }else{
            $('#curBookPriceOffline').attr('readonly' , 'readonly');
        }
    });
});

function getCategory(numberDiv){
    var catalogId = $('#intCatalogID'+numberDiv).val();
    if(catalogId!=""){
        $.ajax({
            url : global_url+"buku/get-list-category/",
            type : "POST",
            data : "id="+catalogId+"&mode=form",
            dataType : "html", 
            success : function msg(res){
                var data = jQuery.parseJSON(res);
                $('#intCategoryID'+numberDiv).html(data);
            }            
        });
    }
}


function saveDataProses(){
    if($('#form-tambah-buku').valid()){
        $('#box-form-tambah-buku').append('<div class="overlay" id="loading-bar"><i class="fa fa-refresh fa-spin"></i></div>');
        $('#box-form-kata-kunci').append('<div class="overlay" id="loading-bar2"><i class="fa fa-refresh fa-spin"></i></div>');
        $('#box-form-kategori').append('<div class="overlay" id="loading-bar3"><i class="fa fa-refresh fa-spin"></i></div>');
        $('#box-form-file').append('<div class="overlay" id="loading-bar4"><i class="fa fa-refresh fa-spin"></i></div>');
    }
    
}


function updateFiling(mode){
    var intPublisherID = $('input[name=intPublisherID]').val();
    var intPublisherBookID = $('input[name=intPublisherBookID]').val();
    $.ajax({
        url : global_url+"buku/pengajuan-review-konversi/",
        type : "POST",
        data : "mode="+mode+"&intPublisherID="+intPublisherID+"&intBookPublisherID="+intPublisherBookID,
        dataType : "html",
        success : function msg(response){
            var data = jQuery.parseJSON(response);
            var status = data['status'];
            var message = data['message'];
            alertPopUp(status,message,"");
            //data_grid.ajax.reload();
            window.location.reload();
            ///var status = data['status'];
            
        }
    });
}

function deleteBooksData(){
    var intPublisherID = $('input[name=intPublisherID]').val();
    var intPublisherBookID = $('input[name=intPublisherBookID]').val();
    bootbox.confirm({
        size : 'small',
        message : "Apakah Anda Akan Menghapus Data Buku Ini?",
        callback : function(response){
            if(response==true){
                $.ajax({
                    url : global_url+"buku/hapus-data-buku/",
                    type : "POST",
                    data : "intPublisherID="+intPublisherID+"&intBookPublisherID="+intPublisherBookID,
                    dataType : "html",
                    success : function msg(response){
                        var data = jQuery.parseJSON(response);
                        var status = data['status'];
                        var message = data['message'];
                        var urlReturn = global_url+"buku/";
                        alertPopUp(status , message , "");
                        data_grid.ajax.reload();    
                    }
                });
            }
        } 
    });

    
}