var base_url_katalog = global_url+'katalog/';
var data_grid;
$(document).ready(function(){
     getListBookCategory();
     var data_grid = $('#data-history-lisensi').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false,
          "iDisplayLength": 5,
          serverSide: true,
          ajax : {
            url : base_url_katalog+"history-lisensi-buku/0/0/0",
            type : "POST"
          }
    });
     
    $('#get-detail-lisensi').click(function(){
        var intLibraryID = $('#intLibraryID').val();
        var txtPublisherServicesID = $('#txtPublisherServicesID').val();
        var txtBookID = $('#txtBookID').val();
        var string_post = intLibraryID+"/"+txtPublisherServicesID+"/"+txtBookID+"/";
        ///alert(string_post);
        var url_post = base_url_katalog+'history-lisensi-buku/'+string_post;
        ////alert(url_post);
        data_grid.ajax.url(url_post).load();
    });
});

function getListBookCategory() {
    //code
    $('#overlay-data-list').css("display" ,"inherit");
    $('#frm-search').css("display" ,"none");
    $.ajax({
       url : base_url_katalog+"get_list_buku/",
       type : "POST",
       data : $('#form-id-katalog').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var htmlRes = data['html'];
            if (status==true) {
                //code
                $('#overlay-data-list').css("display" ,"none");
                $('#frm-search').css("display" ,"inherit");
                $('#result-list-books').html(htmlRes);
            }else{
                $('#result-list-books').html("No Books Available");
            }
       }
    });
}

$('#mymodal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) 
  var id = button.data('id');
  var library = button.data('library');
  var publisher = button.data('publisher');
  $('#details-overlay').css("display" , "inherit");
  $.ajax({
       url : base_url_katalog+"detail_buku/",
       type : "POST",
       data : "intLibraryID="+library+"&txtPublisherServicesID="+publisher+"&txtBookID="+id,
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            console.log(data);
            var modal = $(this)
            modal.find('.modal-title').text('id ' + id);
            var restriction = data['bookRestriction'];
            //modal.find('#img-detail-container').html(data['form_image']);
            var detail_buku = data['bookDetails'];
            $('#intLibraryID').val(library);
            $('#txtPublisherServicesID').val(publisher);
            $('#txtBookID').val(id);
            $('#detail-title').html("Detail Buku "+detail_buku['txtBookTitle']);
            $('#restriction-book-list').html(restriction);
            $('#image-detail').attr('src' , detail_buku['txtUrlImage']);
            $('#deskripsi_buku').html(detail_buku['txtSynopsis']);
            $('#judul_buku').val(detail_buku['txtBookTitle']);
            $('#penerbit').val(detail_buku['txtPublisherServicesID']);
            $('#tahun_terbit').val(detail_buku['intPublishedYear']);
            $('#isbn').val(detail_buku['txtISBN']);
            $('#jumlah_lisensi').val(detail_buku['intTotalBookLicense']);
            //modal.find('.modal-body input').val(recipient)
            $('#details-overlay').css("display" , "none");
            ///getDetailHistory(library,publisher,id);
            
       }
  });
  
  
  
});

function getDetailHistory() {
    //code
    
    var intLibraryID = $('#intLibraryID').val();
    var txtPublisherServicesID = $('#txtPublisherServicesID').val();
    var txtBookID = $('#txtBookID').val();
    var string_post = intLibraryID+"/"+txtPublisherServicesID+"/"+txtBookID+"/";
    ///alert(string_post);
    var url_post = base_url_katalog+'history-lisensi-buku/'+string_post;
    ////alert(url_post);
    data_grid.api().ajax.url(url_post).load();
    alert(url_post);
    
}