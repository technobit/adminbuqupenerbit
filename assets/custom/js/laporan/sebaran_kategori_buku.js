var data_grid_sebaran = "";
$(document).ready(function(){
    data_grid_sebaran = $('#tableSebaranKategori').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": true,
          "processing" : true,
           ajax : {
           url : global_url+"laporan/loadBookList/",
           type : "POST",
           dataSrc: 'data',
           data : function(d){
                d.intCatalogID = $('#intCatalogID').val();
                d.intCategoryID = $('#intCategoryID').val();
            }
          }
    });

    $('#viewReport').click(function(){
        data_grid_sebaran.ajax.reload();
    });

});

function getCategory(){
    var catalogId = $('#intCatalogID').val();
    if(catalogId!=""){
        $.ajax({
            url : global_url+"buku/get-list-category/",
            type : "POST",
            data : "id="+catalogId+"&mode=index",
            dataType : "html", 
            success : function msg(res){
                var data = jQuery.parseJSON(res);
                $('#intCategoryID').html(data);
            }            
        });
    }
}