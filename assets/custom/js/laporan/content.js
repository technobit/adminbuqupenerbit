var base_url_laporan = global_url+'laporan/';
var data_grid_sebaran = "";
$(document).ready(function(){
     $('#tableBookStatus').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": true
    });
    $('#datepicker1').datepicker({
      autoclose: true,
	  format: 'dd-MM-yyyy',
    });
    $('#datepicker2').datepicker({
      autoclose: true,
	  format: 'dd-MM-yyyy',
    });

    data_grid_sebaran = $('#tableSebaranKategori').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": true,
           ajax : {
           url : global_url+"laporan/loadBookList/",
           type : "POST",
           dataSrc: 'data',
           data : function(d){
                d.intCatalogID = $('#intCatalogID').val();
                d.intCategoryID = $('#intCategoryID').val();
            }
          }
    });

});

function loadListCategory(idCatalog) {
    //code
    var loading_html = '<div id="overlay-data-perpus" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    $('#optCategory').append(loading_html);
    $.ajax({
              url : base_url_laporan+"loadListCategory/"+idCatalog,
              type : "POST",
              //data : $('#form-filter-perpus').serialize(),
              //dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
				   $('#optCategory').html(data);
              }
       });
   // $('#overlay-data-perpus').remove();*/
}

function loadDaftarBuku() {
    //code
    data_grid_sebaran.ajax.reload();
}

function loadPenjualanBuku() {
    //code
    var loading_html = '<div id="overlay-data-buku" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    $('#tableBookList').append(loading_html);
    $.ajax({
              url : base_url_laporan+"loadBookSeller/",
              type : "POST",
              data : $('#form-date').serialize(),
              //dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
				   $('#tableBookSell').html(data);
				   $('#tableBookSell').DataTable({
					   destroy: true,
						"paging": true,
						"lengthChange": false,
						"searching": false,
						"ordering": false,
						"info": true,
                        "footer" : true,
						"autoWidth": false
					});
              }
       });
 //$('#overlay-data-buku').remove();
}

function loadSebaranPenjualanBuku() {
    //code
    var loading_html = '<div id="overlay-data-buku" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    $('#tableBookList').append(loading_html);
    $.ajax({
              url : base_url_laporan+"loadSebaranBuku/",
              type : "POST",
              data : $('#form-judul-buku').serialize(),
              //dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
				   $('#tableBookList').html(data);
				   $('#tableBookList').DataTable({
					   destroy: true,
						"paging": true,
						"lengthChange": false,
						"searching": false,
						"ordering": true,
						"info": true,
						"autoWidth": false
					});
              }
       });
 //$('#overlay-data-buku').remove();
}
