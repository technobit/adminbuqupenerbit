var data_grid = "";
$(document).ready(function(){
    $('#date1').datepicker({
      autoclose: true,
	  format: 'dd-MM-yyyy',
    });
    $('#date2').datepicker({
      autoclose: true,
	  format: 'dd-MM-yyyy',
    });

    data_grid = $('#tableBookSell').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": true,
          "processing" : true,
           ajax : {
           url : global_url+"laporan/loadBookSeller/",
           type : "POST",
           dataSrc: 'data',
           data : function(d){
                d.date1 = $('#date1').val();
                d.date2 = $('#date2').val();
            },
            
          }
    });
    
});

function loadPenjualanBuku(){
    data_grid.ajax.reload();
}