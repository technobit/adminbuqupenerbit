var base_url_buku = global_url+'buku/';
var intCatalogId = "";
var intCategoryId = "";
var intStatusId = "";
var txtBookTitle = "";
var data_grid = "";
$(function(){
    
    var overlayHtml = getLoadingOverlay();
    $('#box-list-buku').append(overlayHtml);
    data_grid = $('#tableDetailStatus').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": false,
          "autoWidth": false,
           ajax : {
           url : global_url+"laporan/getDetailStatusBuku/",
           type : "POST",
           dataSrc: 'data',
           data : function(d){
                d.intStatusID = $('#intStatusID').val();
            }
          }
    });

    data_grid.on('draw.dt',function(){
        $('#loading-bar').remove();
    });

    $('#btnShowDataBuku').click(function(){
        data_grid.ajax.reload();
    });

});

