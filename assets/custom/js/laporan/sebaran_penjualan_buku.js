var data_grid = "";
$(document).ready(function(){
   
    data_grid = $('#tableBookList').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": true,
          "processing" : true,
           ajax : {
           url : global_url+"laporan/loadSebaranBuku/",
           type : "POST",
           dataSrc: 'data',
           data : function(d){
                d.txtBookTitle = $('#txtBookTitle').val();
            },
            
          }
    });
    
});

function loadSebaranPenjualanBuku(){
    data_grid.ajax.reload();
}