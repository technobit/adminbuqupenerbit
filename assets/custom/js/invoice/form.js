var base_url_invoice = global_url+'invoice/';
$('#btnImgInvoiceImg').click(function(){
        $('#ntxtInvoiceImg').click();
});
$('#ntxtInvoiceImg').change(function(){
    
    var names = $('#ntxtInvoiceImg').prop("files")[0].name;
    $('#imgInvoiceImg').val(names);
    uploadFileToConvert($('#ntxtInvoiceImg') , 'ntxtInvoiceImgTxtDir');
});

function requestInvoice(intCopyLicense,curTotalPurchase,intYear,intMonth){
    var txtInvoiceNumber = $('#txtInvoiceNumber').val();
    //var ntxtInvoiceProof = $('#ntxtFrontCoverTxtDir').val();

    msgAlert = '<p>Pengajuan Invoice sedang diproses <i class="fa fa-refresh fa-spin"></i></p>';
    $("#alertMsgSuccess").html(msgAlert);
    $('#success').modal('show');

    if (txtInvoiceNumber != ''){
    $.ajax({
        url : base_url_invoice+"requestInvoice/",
        type : "POST",
        data :  $('#formInvoiceRequest').serialize()+"&intCopyLicense="+intCopyLicense+"&curTotalPurchase="+curTotalPurchase+"&intYear="+intYear+"&intMonth="+intMonth,
        dataType : "html", 
        success : function msg(res){
            var data = jQuery.parseJSON(res);
            dataAlert = data['0'].bitSuccess;
            if (dataAlert == 1){
                $('#success').modal('hide');
                $('#failed').modal('hide');
                msgAlert = 'Permintaan Invoice berhasil diajukan';
                $("#alertMsgSuccess").html(msgAlert);
                $('#success').modal('show');
                    setTimeout(function () {
                        window.location.href = base_url_invoice;
                        }, 1000);
            }else{
                $('#success').modal('hide');
                $('#failed').modal('hide');
                msgAlert = '<p>Permintaan Invoice gagal diajukan</p>';
                $("#alertMsgFailed").html(msgAlert);
                $('#failed').modal('show');
                //$('#failed').modal('show');
                    /*setTimeout(function () {
                        location.reload();
                        }, 1000);*/
            }
             //alert(data);
        }           
    });
    }else{
        $('#success').modal('hide');
        $('#failed').modal('hide');
        msgAlert = '<p>Nomor Invoice harus diisi</p>';
        $("#alertMsgFailed").html(msgAlert);
		$('#failed').modal('show');
    }
}

function uploadFileToConvert(uploadDiv , imageDirSave){
    var file_data = uploadDiv.prop("files")[0];
    var form_data = new FormData();  
    form_data.append("fileImage", file_data);
    $.ajax({
        url : base_url_invoice+"uploadToConvert/",
        type : "POST",
        contentType: false,
        processData: false,
        cache: true,
        data : form_data,
        success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            if(status==true){
                var imageFile = data['temp_image_dir']; 
                $('input[name='+imageDirSave+']').val(imageFile);
            }
        }
    });
}
