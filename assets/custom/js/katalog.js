var base_url_katalog = global_url+'katalog/';

$(document).ready(function(){
     $('#data-perpustakaan').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
    });
});

function getDataPerpustakaan() {
    //code
    var loading_html = '<div id="overlay-data-perpus" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    $('#box-result-perpus').append(loading_html);
    $.ajax({
              url : base_url_katalog+"get-katalog-library/",
              type : "POST",
              data : $('#form-filter-perpus').serialize(),
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var html = data['html'];
                   if (status==true) {
                     //code
                            $('#list-data-perpustakaan').html(html);
                            
                   }else{
                            $('#list-data-perpustakaan').html("");
                   }
                   
              }
       });
    $('#overlay-data-perpus').remove();
}