var base_url_deposit = global_url+'deposit/detail/';

$(document).ready(function(){
$('#transaksi-deposit').DataTable({
          "paging": false,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false
    });
getDataHistoryDeposit();
});

function getDataHistoryDeposit(){
    //code
    $.ajax({
              url : base_url_deposit+"histori-transaksi/",
              type : "POST",
              data : $('#frm-detail-deposit').serialize(),
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var html = data['html'];
                   if (status==true) {
                     //code
                            $('#result-transaksi-deposit').html(html);
                   }else{
                            $('#result-transaksi-deposit').html("");
                   }
                   
              }
       });
    
}

function addDeposit() {
    //code
    $('#progress').css('display','inherit');
    $.ajax({
              url : base_url_deposit+"tambah-deposit/",
              type : "POST",
              data : $('#frm-detail-deposit').serialize(),
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var message = data['message'];
                   if (status==true) {
                     //code
                            $('#msg-data-success').html(message);
                            $('#warning-success').css('display','inherit');
                            getDataHistoryDeposit();
                   }else{
                            $('#msg-data-failed').html(message);
                            $('#warning-failed').css('display','inherit');
                   }
                $('#progress').css('display','none');               
              }
       });
    
}

function validTransaction(intLibraryID , intTrPaymentGatewayID) {
    //code
    $('#progress').css('display','inherit');
    $.ajax({
              url : base_url_deposit+"valid-deposit/",
              type : "POST",
              data : "intLibraryID="+intLibraryID+"&intTrPaymentGatewayID="+intTrPaymentGatewayID,
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var message = data['message'];
                   if (status==true) {
                     //code
                            $('#msg-data-success').html(message);
                            $('#warning-success').css('display','inherit');
                            getDataHistoryDeposit();
                   }else{
                            $('#msg-data-failed').html(message);
                            $('#warning-failed').css('display','inherit');
                   }
                $('#progress').css('display','none');               
              }
       });
}