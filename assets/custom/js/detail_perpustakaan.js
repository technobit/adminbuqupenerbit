var base_url_perpustakaan = global_url+'perpustakaan/';

$('#add-admin-perpus').click(function(){
    $('#frm-detail-admin')[0].reset();
    $('#modal-admin-perpus').modal('show');
});
$(document).ready(function() {
    getDataTokoBuku();
    getDataAdminPerpus();
    getListTokoBuku();
    
    
    $('#toko-buku').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
    });
    
    $('#btn-save-admin-perpus').click(function(e){
        e.preventDefault();
        if ($('#frm-detail-admin').valid()) {
            //code
            saveDataAdminPerpus();
        }
    });

    $('#frm-detail-admin').validate({
        ignore : "",
        rules : {
                     username_admin : {
                            required : true,
                     },
                     email_admin : {
                            required : true,
                            email : true
                     },
              }
    });
});

function getDataAdminPerpus() {
    //code
    var loading_html = '<div id="overlay-admin" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    $('#box-admin-perpus').append(loading_html);
    var id = $('input[name=intLibraryID]').val();
    $.ajax({
              url : base_url_perpustakaan+"get_admin_perpustakaan/",
              type : "POST",
              data : "id="+id,
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var html = data['html'];
                   if (status==true) {
                     //code
                            $('#list_admin_perpus').html(html);
                   }else{
                            $('#list_admin_perpus').html("");
                   }
                   
              }
       });
        $('#overlay-admin').remove();
}

function saveDataAdminPerpus() {
    //code
    $.ajax({
              url : base_url_perpustakaan+"save_admin_perpustakaan/",
              type : "POST",
              data : $('#frm-detail-admin').serialize(),
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var message = data['message'];
                   htmlDiv = alertMessage(message , status);
                   if (status==true) {
                     //code
                            getDataAdminPerpus();
                            $('#msg-header-admin').html(htmlDiv);
                            $('#modal-admin-perpus').modal("hide");
                   }else{
                            $('#msg-header-admin').html(htmlDiv);
                            $('#modal-admin-perpus').modal("hide");
                   }
                   
              }
       });
}

function detailAdmin(id_library , id_staff) {
    //code
    $.ajax({
              url : base_url_perpustakaan+"detail_admin_perpustakaan/",
              type : "POST",
              data : "intLibraryID="+id_library+"&intLibraryStaffID="+id_staff,
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var message = data['message'];
                   if (status==true) {
                     //code
                        
                        var data_admin = data['data_admin'];
                        var status = (data_admin['bitActive']==true ? "1" : "0");
                        $('#intLibraryStaffID').val(data_admin['intLibraryStaffID']);
                        $('#txtUserName').val(data_admin['txtUserName']);
                        $('#txtEmail').val(data_admin['txtEmail']);
                        $('#bitActive').val(status);
                        $('#modal-admin-perpus').modal('show');
                        $('#btn-save-admin-perpus').html('Update');
                   }else{
                            
                   }
                   
              }
       });
}

function hapusAdmin(id_library , id_staff) {
    //code
    var r = confirm("Apakah Anda Akan Menghapus Data???");
    if (r==true) {
        //code
        $.ajax({
              url : base_url_perpustakaan+"hapus_admin_perpustakan/",
              type : "POST",
              data : "intLibraryID="+id_library+"&intLibraryStaffID="+id_staff,
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var message = data['message'];
                   htmlDiv = alertMessage(message , status);
                   if (status==true) {
                     //code
                            getDataAdminPerpus();
                            $('#msg-header-admin').html(htmlDiv);
                   }else{
                            $('#msg-header-admin').html(htmlDiv);
                   }
                   
              }
       });
    }
    
}


function resetAdmin(id_library , email_staff) {
    //code
    var r = confirm("Apakah Anda Akan Me-'Reset Password Admin??' ");
    if (r==true) {
        //code
        $.ajax({
              url : base_url_perpustakaan+"reset_password_admin/",
              type : "POST",
              data : "intLibraryID="+id_library+"&txtEmail="+email_staff,
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var message = data['message'];
                   htmlDiv = alertMessage(message , status);
                   if (status==true) {
                     //code
                            getDataAdminPerpus();
                            $('#msg-header-admin').html(htmlDiv);
                   }else{
                            $('#msg-header-admin').html(htmlDiv);
                   }
                   
              }
       });
    }
    
}

function saveTokoBuku() {
    //code
    var loading_html = '<div id="overlay-buku" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    $('#box-toko-buku').append(loading_html);
    $.ajax({
              url : base_url_perpustakaan+"save_toko_buku_perpus/",
              type : "POST",
              data : $('#frm-toko-buku').serialize(),
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var message = data['message'];
                   htmlDiv = alertMessage(message , status);
                   if (status==true) {
                     //code
                            getDataTokoBuku();
                            $('#msg-header-toko-buku').html(htmlDiv);
                   }else{
                            $('#msg-header-toko-buku').html(htmlDiv);
                   }
                   
              }
       });
    $('#overlay-buku').remove();
}

function getListTokoBuku() {
    //code
    var loading_html = '<div id="overlay-list-buku" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    $('#box-list-toko-buku').append(loading_html);
     $.ajax({
              url : base_url_perpustakaan+"get_list_toko_buku/",
              type : "POST",
              dataType : "html",
              success: function msg(res){
                
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var html = data['html'];
                   if (status==true) {
                     //code
                            $('#list_toko_buku').html(html);
                            
                   }else{
                            $('#list_toko_buku').html("");
                            
                   }
                   $('.select2').select2();
              }
       });
    $('#overlay-list-buku').remove();
    
}

function getDataTokoBuku() {
    //code
    var loading_html = '<div id="overlay-buku" class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
    $('#box-toko-buku').append(loading_html);
    var id = $('input[name=intLibraryID]').val();
    $.ajax({
              url : base_url_perpustakaan+"get_toko_buku_perpustakaan/",
              type : "POST",
              data : "id="+id,
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var html = data['html'];
                   if (status==true) {
                     //code
                            $('#list_toko_buku_perpus').html(html);
                            $('#overlay-buku').remove();
                   }else{
                            $('#list_toko_buku_perpus').html("");
                            
                   }
                   
                   
              }
       });
    $('#overlay-buku').remove();
}



function hapusTokoBuku(id_library , toko_buku) {
    //code
    var r = confirm("Apakah Anda Akan Menghapus Data???");
    if (r==true) {
        //code
        $.ajax({
              url : base_url_perpustakaan+"hapus_toko_buku_perpustakaan/",
              type : "POST",
              data : "intLibraryID="+id_library+"&txtBookStoreServicesID="+toko_buku,
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var message = data['message'];
                   htmlDiv = alertMessage(message , status);
                   if (status==true) {
                     //code
                            getDataTokoBuku();
                            $('#msg-header-toko-buku').html(htmlDiv);
                   }else{
                            $('#msg-header-toko-buku').html(htmlDiv);
                   }
                   
              }
       });
    }
    
}