var link_penerbit = global_url+"penerbit/";
$(document).ready(function() {
    $('#btnUpdatePenerbit').click(function(e){
       e.preventDefault();
       if($('#frm-detail-penerbit').valid()){
           saveUpdatePenerbit();
       } 
    });
    $('#frm-detail-penerbit').validate({
        ignore : "",
        rules : {
            txtPublisherGroup : {
                required : true,
            },
            txtPublisherName : {
                required : true,
            },
            txtEmail : {
                required : true,
            },
        }
    })
});

function saveUpdatePenerbit(){
    $.ajax({
              url : link_penerbit+"update-data/",
              type : "POST",
              data : $('#frm-detail-penerbit').serialize(),
              dataType : "html",
              success: function msg(res){
                   var data = jQuery.parseJSON(res);
                   var status = data['status'];
                   var message = data['message'];
                   alertPopUp(status , message , link_penerbit);
              }
       });
    
}